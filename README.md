# Content of this repository
This repository contains a library with common functionalities used in multiple projects of the [dotstatsuite-core](https://gitlab.com/sis-cc/.stat-suite). Among some of the functionalities:
*  **It defines the model of the general configuration** of the [dotstatsuite-core-transfer service](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer), the [dotstatsuite-core-sdmxri-nsi-plugin](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin) and the [dotstatsuite-core-auth-management service](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management)   
*  **It provides a common logging library** 

>  The content of this repository is not meant to be used as standalone application. 

>  **This library is publically published as a nuget package named [DotStat.Common](https://www.nuget.org/packages/DotStat.Common)**

The following solutions have **direct dependencies** to this library:
*  [dotstatsuite-core-data-access](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access)

The following solutions have **non-direct dependencies** to this library:
*  [dotstatsuite-core-auth-management](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management)
*  [dotstatsuite-core-transfer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer)
*  [dotstatsuite-core-sdmxri-nsi-plugin](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin)