# Log4Net 
[**Log4Net library**](https://logging.apache.org/log4net/) is used by the dotstatsuite-core-transfer and the NSI web service to log all their activity. This library is also used by dependencies like Estat.Sri, Org.Sdmx among others. 

## Configuration 
### **dotstatsuite-core-transfer** - Default log4net configuration 
The dotstatsuite-core-transfer service obtains the log4net donfiguration from the file `config/log4net.config`

By default it is configured to log the outputs to two files:
*  `Logs\dotstatsuite-core-transfer-system.log`  **System Activity** not related to user interaction, for example the service startup and initialization
*  `Logs\dotstatsuite-core-transfer-user.log` **User activity** related to user interaction, for example a new data import.

>  **System activity** will log at ALL levels 
>  **User activity** will log at WARNNING level.

```xml
<?xml version="1.0" encoding="utf-8"?>
<log4net>
  <appender name="SystemActivityAppender" type="log4net.Appender.RollingFileAppender">
   <!--Output file -->
    <file type="log4net.Util.PatternString" value="logs/dotstatsuite-core-transfer-system.log"/>
    <rollingStyle value="Date"/>
    <datePattern value="yyyyMMdd"/>
    <!--enable the following setting to keep a max number of backup log files.-->
    <!--<maxSizeRollBackups value="10" />-->
    <!--<maximumFileSize value="100MB" />-->
    <staticLogFileName value="true" />
    <appendToFile value="true"/>
    <layout type="log4net.Layout.PatternLayout">
      <!--Add of remove fields to adjust the logging information, note that "|" is being used as delimiter-->
      <conversionPattern value="%date|[%thread]|%-5level|%property{log4net:HostName}|%logger|%message|%exception%newline" />
    </layout>
    <!--Filter system activity-->
    <filter type="log4net.Filter.PropertyFilter">
        <key value="DataSpaceId" />
        <StringToMatch value="null" />
    </filter>
    <filter type="log4net.Filter.DenyAllFilter" />
  </appender>
  
  <appender name="UserActivityAppender" type="log4net.Appender.RollingFileAppender">
   <!--Output file -->
    <file type="log4net.Util.PatternString" value="logs/dotstatsuite-core-transfer-user.log"/>
    <rollingStyle value="Date"/>
    <datePattern value="yyyyMMdd"/>
    <!--enable the following setting to keep a max number of backup log files.-->
    <!--<maxSizeRollBackups value="10" />-->
    <maximumFileSize value="100MB" />
    <staticLogFileName value="true" />
    <appendToFile value="true"/>
    <layout type="log4net.Layout.PatternLayout">
      <!--Add of remove fields to adjust the logging information, note that "|" is being used as delimiter-->
      <conversionPattern value="%date|[%thread]|%property{DataSpaceId}|%property{TransactionId}|%-5level|%property{log4net:HostName}|%property{Application}|%logger|%message|%exception|%property{UrlReferrer}|%property{HttpMethod}|%property{RequestPath}|%property{QueryString}%newline" />
    </layout>
	<!--Log level WARNNING -->
	<Threshold value="WARN" />
    <!--Filter user activity-->
    <filter type="log4net.Filter.PropertyFilter">
        <key value="DataSpaceId" />
        <StringToMatch value="null" />
        <acceptOnMatch value="false" />
    </filter>
  </appender>
  <root>
    <!-- Options are "ALL", "DEBUG", "INFO", "NOTICE", "WARN", "ERROR", "FATAL" and "OFF". -->
    <level value="ALL"/>
    <appender-ref ref="SystemActivityAppender"/>
    <appender-ref ref="UserActivityAppender"/>
  </root>
</log4net>
```

* To change the output file, modify the value at `<file>` element. 
* To change the output format, modify the value at the `<conversionPattern>` element. Note that "|" is being used as delimiter. ([See more about parameters](#logging-parameters))
* To change the loging level, modify the value of the `<level>` element. ([See more about logging levels](#logging-levels))

### Configuration Examples
#### Log to all activity to a single file
The following example shows a basic configuration, where all activity is logged to a single file, using "," as delimiter and log at "WARN" level.

```xml
<?xml version="1.0" encoding="utf-8"?>
<log4net>
  <appender name="AllActivityAppender" type="log4net.Appender.RollingFileAppender">
   <!--Output file -->
    <file type="log4net.Util.PatternString" value="logs/customFileName.log"/>
    <rollingStyle value="Date"/>
    <datePattern value="yyyyMMdd"/>
    <appendToFile value="true"/>
    <staticLogFileName value="true" />
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%date|[%thread]|%property{DataSpaceId}|%property{TransactionId}|%-5level|%property{log4net:HostName}|%logger|%message|%exception|%property{UrlReferrer}|%property{HttpMethod}|%property{RequestPath}|%property{QueryString}%newline" />
    </layout>
  </appender>
  <root>
    <!-- Options are "ALL", "DEBUG", "INFO", "NOTICE", "WARN", "ERROR", "FATAL" and "OFF". -->
    <level value="ERROR"/>
    <appender-ref ref="AllActivityAppender"/>
  </root>
</log4net>
```


#### Log to database database example
dotstatsuite-core contains one data database (```DotStatSuiteCore_Data_\<dataspace\>```) per dataspace ([see data DB model](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access?nav_source=navbar#dotstatsuitecore_data_dataspace-database)), which contains a pre-defined table for logging purposes[ [Management].[LOGS]](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access?nav_source=navbar#managementlogs)

The following example shows an advanced configuration for a dotstatsuite-core-transfer service installation with two dataspaces ("design" and "disseminate"). 
The logs will be stored in three different places depending on the type of activity and the data space where it is taking place:
*  **Service's activity**.- Activity not related to user interaction, for example the service startup and initialization
    *   Log level ALL
    *   Log to file `file logs/dotstatsuite-core-transfer-system.log`
*  **User Activity**.- Activity related to user interaction, for example a new data import.
    1.  "design" data space.
        *  Log level NOTICE
        *  Log to the table [Management].[LOGS] inside the ```DotStatSuiteCore_Data_design``` database. 
    2.  "disseminate"
        *  Log level WARNING
        *  Log to the table [Management].[LOGS] inside the ```DotStatSuiteCore_Data_disseminate``` database. 

```xml
<?xml version="1.0" encoding="utf-8"?>
<log4net>
  <appender name="SystemActivityAppender" type="log4net.Appender.RollingFileAppender">
    <!--Dynamic file name, where %processid is the process id number-->
    <file type="log4net.Util.PatternString" value="logs/dotstatsuite-core-transfer-system.log"/>
    <rollingStyle value="Date"/>
    <datePattern value="yyyyMMdd"/>
    <appendToFile value="true"/>
    <layout type="log4net.Layout.PatternLayout">
      <!--Add of remove fields to adjust the logging information, note that "|" is being used as delimiters-->
      <conversionPattern value="%date|[%thread]|%-5level|%property{log4net:HostName}|%logger|%message|%exception|%property{UrlReferrer}|%property{HttpMethod}|%property{RequestPath}|%property{QueryString}%newline" />
    </layout>
	  <!--Exclude activity on "design" and "disseminate" dataspace -->
      <filter type="log4net.Filter.PropertyFilter">
        <key value="DataSpaceId" />
        <StringToMatch value="design" />
        <acceptOnMatch value="false" />
      </filter>
      <filter type="log4net.Filter.PropertyFilter">
        <key value="DataSpaceId" />
        <StringToMatch value="disseminate" />
        <acceptOnMatch value="false" />
      </filter>
      <!--
        The following filters are used by the  dotstatsuite-core-sdmxri-nsi-plugin
        to filter activity above it (NSIWS) and by lower level dependencies
      -->
	 <!--Exclude activity from HTTPMethods -->
	 <filter type="log4net.Filter.PropertyFilter">
        <key value="HttpMethod" />
        <StringToMatch value="GET" />
        <acceptOnMatch value="false" />
      </filter>
      <filter type="log4net.Filter.PropertyFilter">
        <key value="HttpMethod" />
        <StringToMatch value="POST" />
        <acceptOnMatch value="false" />
      </filter>
	 <!--Exclude activity from Lower dependencies -->
     <filter type="log4net.Filter.LoggerMatchFilter">
        <loggerToMatch value="Org.Sdmxsource" />
        <acceptOnMatch value="false" />
      </filter>
      <filter type="log4net.Filter.LoggerMatchFilter">
        <loggerToMatch value="Estat.Sdmxsource" />
        <acceptOnMatch value="false" />
      </filter>
      <filter type="log4net.Filter.LoggerMatchFilter">
        <loggerToMatch value="Estat.Sri.MappingStoreRetrieval" />
        <acceptOnMatch value="false" />
      </filter>
      <filter type="log4net.Filter.LoggerMatchFilter">
        <loggerToMatch value="org.estat.sri.sqlquerylogger" />
        <acceptOnMatch value="false" />
      </filter>
  </appender>
  
  <appender name="DesignDataspaceAppender" type="MicroKnights.Logging.AdoNetAppender, MicroKnights.Log4NetAdoNetAppender">
    <useTransactions value="false" />
    <bufferSize value="1" />
    <connectionType value="System.Data.SqlClient.SqlConnection, System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <commandText value="INSERT INTO [Management].Logs ([Date],[Thread],[Transaction_Id],[Level],[Server],[Application],[Logger],[Message],[Exception],[UrlReferrer],[HttpMethod],[RequestPath],[QueryString]) VALUES (@log_date, @thread, @transactionId, @log_level, @server, @application, @Logger, @message, @exception,@urlReferrer,@httpMethod,@requestPath,@queryString)" />
    <connectionStringName value="ConnectionString1" />
	<!--ALLOW LOGS ONLY AT NOTICE LEVEL-->
	<Threshold value="NOTICE" />
    <parameter>
      <parameterName value="@log_date" />
      <dbType value="DateTime" />
      <layout type="log4net.Layout.RawTimeStampLayout" />
    </parameter>
    <parameter>
      <parameterName value="@thread" />
      <dbType value="String" />
      <size value="255" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%thread" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@DataSpaceId" />
      <dbType value="String" />
      <size value="255" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="DataSpaceId" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@TransactionId" />
      <dbType value="Int32" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="TransactionId" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@log_level" />
      <dbType value="String" />
      <size value="50" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%level" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@server" />
      <dbType value="String" />
      <size value="50" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%property{log4net:HostName}" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@Application" />
      <dbType value="String" />
      <size value="255" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%property{Application}" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@Logger" />
      <dbType value="String" />
      <size value="255" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%logger" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@message" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%message" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@exception" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.ExceptionLayout" />
    </parameter>
    <parameter>
      <parameterName value="@UrlReferrer" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="UrlReferrer" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@HttpMethod" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="HttpMethod" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@RequestPath" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="RequestPath" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@QueryString" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="QueryString" />
      </layout>
    </parameter>
	<!--Filter to activity in the "design" dataspace context -->
    <filter type="log4net.Filter.PropertyFilter">
      <key value="DataSpaceId" />
      <StringToMatch value="design" />
    </filter>
    <filter type="log4net.Filter.DenyAllFilter" />
  </appender>
  <appender name="DisseminateDataspaceAppender" type="MicroKnights.Logging.AdoNetAppender, MicroKnights.Log4NetAdoNetAppender">
    <useTransactions value="false" />
    <bufferSize value="1" />
    <connectionType value="System.Data.SqlClient.SqlConnection, System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <commandText value="INSERT INTO [Management].Logs ([Date],[Thread],[Transaction_Id],[Level],[Server],[Application],[Logger],[Message],[Exception],[UrlReferrer],[HttpMethod],[RequestPath],[QueryString]) VALUES (@log_date, @thread, @transactionId, @log_level, @server, @application, @Logger, @message, @exception,@urlReferrer,@httpMethod,@requestPath,@queryString)" />
    <connectionStringName value="ConnectionString1" />
	<!--ALLOW LOGS ONLY AT WARNNING LEVEL-->
	<Threshold value="WARN" />
    <parameter>
      <parameterName value="@log_date" />
      <dbType value="DateTime" />
      <layout type="log4net.Layout.RawTimeStampLayout" />
    </parameter>
    <parameter>
      <parameterName value="@thread" />
      <dbType value="String" />
      <size value="255" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%thread" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@DataSpaceId" />
      <dbType value="String" />
      <size value="255" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="DataSpaceId" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@TransactionId" />
      <dbType value="Int32" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="TransactionId" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@log_level" />
      <dbType value="String" />
      <size value="50" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%level" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@server" />
      <dbType value="String" />
      <size value="50" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%property{log4net:HostName}" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@Application" />
      <dbType value="String" />
      <size value="255" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%property{Application}" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@Logger" />
      <dbType value="String" />
      <size value="255" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%logger" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@message" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%message" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@exception" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.ExceptionLayout" />
    </parameter>
    <parameter>
      <parameterName value="@UrlReferrer" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="UrlReferrer" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@HttpMethod" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="HttpMethod" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@RequestPath" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="RequestPath" />
      </layout>
    </parameter>
    <parameter>
      <parameterName value="@QueryString" />
      <dbType value="String" />
      <size value="-1" />
      <layout type="log4net.Layout.RawPropertyLayout">
        <key value="QueryString" />
      </layout>
    </parameter>
	<!--Filter to activity in the "disseminate" dataspace context -->
    <filter type="log4net.Filter.PropertyFilter">
      <key value="DataSpaceId" />
      <StringToMatch value="disseminate" />
    </filter>
    <filter type="log4net.Filter.DenyAllFilter" />
  </appender>
  
   
  <root>
    <!-- Options are "ALL", "DEBUG", "INFO", "NOTICE", "WARN", "ERROR", "FATAL" and "OFF". -->
    <level value="ALL"/>
    <appender-ref ref="SystemActivityAppender"/>
    <appender-ref ref="designDataspaceAppender"/>
    <appender-ref ref="disseminateDataspaceAppender"/>
  </root>
</log4net>
```

**Please note that no connection string has been defined in the configuration above**. 

**The service will automatically add the connection string** if the following conditions apply:

*  If there are database appenders in the configuration with the appender's name having the structure **\<dataspace\>DataspaceAppender** *(example DesignDataspaceAppender)*.
*  And the appender has no connection string specified.

>  The service checks if the dataspace exists in the configuration file **dataspaces.private.json** and uses the value of "DotStatSuiteCoreDataDbConnectionString" for that specific dataspace. ([See configuration](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer?nav_source=navbar#configuration))


To change the logs of specific data space to a different database, simply add the connection string
```xml
...
  <appender name="DesignDataspaceAppender" type="MicroKnights.Logging.AdoNetAppender, MicroKnights.Log4NetAdoNetAppender">
    ...
    <connectionStringName value="ConnectionString1" />
    <connectionString value="Data Source=localhost;Initial Catalog=COMMON_DB;User ID=USERNAME;Password=XXXXXX" />
    ...
  </appender>
```

### Automatic logging to database
As seen in the previous example, the logging configuration can get extensive quite easily, especially if the service has more than one dataspace.

To automate this configuration process the automatic logging to database can be set for all the configured dataspaces.
> The logging will have the same behavior as the previous example, and the configuration will be dynamic via auto-generated log4net runtime objects rather than being statically defined in the config file.

How to set the automatic logging to database:
*  The log4net configuration file `config/log4net.config` should be left as is.
*  In the configuration file containing the dataspace definitions (e.g. `dataspaces.private.json`) for each dataspace, inside the "spacesInternal", add the option **AutoLog2DB** as true 

Example:

dataspaces.private.json
```json
{
  "spacesInternal": [
    {
      "Id": "design",
      "DotStatSuiteCoreStructDbConnectionString": "Data Source=localhost;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX",
      "DotStatSuiteCoreDataDbConnectionString": "Data Source=localhost;Initial Catalog=DATA_DB;User ID=USERNAME;Password=XXXXXX",
      "DataImportTimeOutInMinutes": 60,
      "AuthEndpoint": "",
      "DatabaseCommandTimeoutInSec": 60,
      "AutoLog2DB" : true,
      "AutoLog2DBLogLevel" : "notice",
      "EmailLogLevel" : "notice"
    },
    {
      "Id": "disseminate",
      "DotStatSuiteCoreStructDbConnectionString": "Data Source=localhost;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX",
      "DotStatSuiteCoreDataDbConnectionString": "Data Source=localhost;Initial Catalog=DATA_DB;User ID=USERNAME;Password=XXXXXX",
      "DataImportTimeOutInMinutes": 60,
      "AuthEndpoint": "",
      "DatabaseCommandTimeoutInSec": 60,
      "AutoLog2DB" : true,
      "AutoLog2DBLogLevel" : "notice",
      "EmailLogLevel" : "notice"	  
    }
  ]
}
```
>  Note that only the dataspaces with **"AutoLog2DB"= true** will log to their corresponding *DotStatSuiteCoreDataDbConnectionString* database. The default loglevel of AutoLog2DB is "NOTICE" which can be changed at the **"AutoLog2DBLogLevel"** parameter of the dataspace configuration.

>  If **"AutoLog2DB"= `false`** or is not included in the configuration, the logs for the specific dataspace will remain in the file "logs/dotstatsuite-core-transfer-user.log"




### **NSI web service** - Default log4net configuration 
Please read the official NSIWS.NET [logging configuration](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/CONFIGURATION.md) 

When using the dotstatsuite-core-sdmxri-nsi-plugin in the NSI web service, the **Automatic logging to database** feature can also be **set to ON**. 
This can be achieved by modifying the dataspaces.private.json and setting **"AutoLog2DB"= `true`**.

This will also separate the NSIWS's system activity and user activity (to data database).

### **[See more configuration examples from Log4net official documentation](https://logging.apache.org/log4net/release/config-examples.html)**

## Logging Parameters
### Log4net Parameters
Log4net has a [list of parameters](https://logging.apache.org/log4net/log4net-1.2.13/release/sdk/log4net.Layout.PatternLayout.html) that are recognized conversion patterns that can be used out of the box to modify the output format.
### Custom Parameters
The following list contains an extension to the log4net parameters, introduced by the dotstatsuite-core-transfer service and the dotstatsuite-core-sdmxri-nsi-plugin:

|Parameter|Description|
| ------ | ------ |
|%property{DataSpaceId}|From dataspaces.private.json, is the unique identifier for the data space, ex. "design". nsi-ws will have a single dataspaceid, while the transfer can have multiple.|
|%property{TransactionId}|Unique ID of the transaction (Only available in the dotstatsuite-core-transfer service)|
|%property{Application}|The name of the application that generated the log entry (dotstatsuite-core-transfer or nsi-ws).|
|%property{UrlReferrer}|From the httpcontext HttpRequest.HttpMethod Property,gets information about the URL of the client's previous request that linked to the current URL.|
|%property{HttpMethod}|From the httpcontext HttpRequest.HttpMethod Property, gets the HTTP data transfer method (such as GET, POST, or HEAD) used by the client.|
|%property{RequestPath}|From the httpcontext HttpRequest.HttpMethod Property, gets the virtual path of the current request.|
|%property{QueryString}|From the httpcontext HttpRequest.HttpMethod Property, gets the collection of HTTP query string variables.|
|%property{MemberName}|From System.Runtime.CompilerServices, is the method or property name of the caller to the method|
|%property{FilePath}|From System.Runtime.CompilerServices, is the full path of the source file that contains the caller. This is the file path at the time of compile.|
|%property{LineNumber}|From System.Runtime.CompilerServices, is the line number in the source file at which the method is called.|


## Logging Levels
Setting|Description
---|---
Off|Turn off logging
FATAL|Log only critical level messages
ERROR|Log only critical and error messages
WARN|Log only critical, error and warning messages
NOTICE| Log notice, warning, error and fatal messages - Messages to users about transactions (e.g. about file import or data transfer between spaces) are logged at this level
INFO|Log information, notice, warning, error and fatal messages
DEBUG|Log debug messages and above (Info, Notice, Warn, Error and Fatal)
ALL|Log all messages
