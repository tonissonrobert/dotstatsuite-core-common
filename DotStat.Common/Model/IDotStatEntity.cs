﻿
namespace DotStat.Common.Model
{
    public interface IDotStatEntity<T> where T:struct
    {
        T Id { get; set; }
    }
}
