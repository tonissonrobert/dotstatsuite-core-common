﻿using System.Linq;
using System.Text;

namespace DotStat.Common.Util
{
    public static class UrlHelper
    {
        public static string Combine(string baseUrl, params string[] fragments)
        {
            var sb = new StringBuilder();
            sb.Append(baseUrl.TrimEnd('/'));

            foreach (var fragment in fragments.Where(s => !string.IsNullOrEmpty(s))
                .Select(s => s.Trim('/')).Where(s => !string.IsNullOrEmpty(s))
                .Select(s => EncodingHelper.UrlEncode(s)))
            {
                sb.Append('/');
                sb.Append(fragment);
            }

            return sb.ToString();
        }
    }
}