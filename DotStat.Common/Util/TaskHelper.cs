﻿using System;
using System.Threading.Tasks;

namespace DotStat.Common.Util
{
    public static class TaskHelper
    {
        public static Task<TResult> FromException<TResult>(Exception e)
        {
            var source = new TaskCompletionSource<TResult>();
            source.SetException(e);
            return source.Task;
        }

        public static void WaitSafe(Task task)
        {
            task.ContinueWith(t => { }).Wait();
        }

        public static void WaitAllSafe(Task[] tasks)
        {
            Task.WhenAll(tasks).ContinueWith(t => { }).Wait();
        }
    }
}