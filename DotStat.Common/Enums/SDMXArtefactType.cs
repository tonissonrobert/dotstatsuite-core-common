﻿
namespace DotStat.Common.Enums
{
    public enum SDMXArtefactType
    {
        /// <summary>
        ///     The any.
        /// </summary>
        Any,

        // BASE

        /// <summary>
        ///     The agency scheme.
        /// </summary>
        AgencyScheme,

        /// <summary>
        ///     The agency.
        /// </summary>
        Agency,

        /// <summary>
        ///     The data provider scheme.
        /// </summary>
        DataProviderScheme,

        /// <summary>
        ///     The data provider.
        /// </summary>
        DataProvider,

        /// <summary>
        ///     The data consumer scheme.
        /// </summary>
        DataConsumerScheme,

        /// <summary>
        ///     The data consumer.
        /// </summary>
        DataConsumer,

        /// <summary>
        ///     The organisation unit scheme.
        /// </summary>
        OrganisationUnitScheme,

        /// <summary>
        ///     The organisation unit.
        /// </summary>
        OrganisationUnit,

        // CODELIST

        /// <summary>
        ///     The code list.
        /// </summary>
        CodeList,

        /// <summary>
        ///     The code.
        /// </summary>
        Code,

        /// <summary>
        ///     The hierarchical codelist.
        /// </summary>
        HierarchicalCodelist,

        /// <summary>
        ///     The hierarchy.
        /// </summary>
        Hierarchy,

        /// <summary>
        ///     The hierarchical code.
        /// </summary>
        HierarchicalCode,

        // CATEGORY SCHEME

        /// <summary>
        ///     The categorisation.
        /// </summary>
        Categorisation,

        /// <summary>
        ///     The category scheme.
        /// </summary>
        CategoryScheme,

        /// <summary>
        ///     The category.
        /// </summary>
        Category,

        // CONCEPT SCHEME

        /// <summary>
        ///     The concept scheme.
        /// </summary>
        ConceptScheme,

        /// <summary>
        ///     The concept.
        /// </summary>
        Concept,

        // DATA STRUCTURE

        /// <summary>
        ///     The DSD.
        /// </summary>
        Dsd,

        /// <summary>
        ///     The data attribute.
        /// </summary>
        DataAttribute,

        /// <summary>
        ///     The attribute descriptor.
        /// </summary>
        AttributeDescriptor,

        /// <summary>
        ///     The dataflow.
        /// </summary>
        Dataflow,

        /// <summary>
        ///     The dimension.
        /// </summary>
        Dimension,

        /// <summary>
        ///     The group.
        /// </summary>
        Group,

        /// <summary>
        ///     The measure dimension.
        /// </summary>
        MeasureDimension,

        /// <summary>
        ///     The time dimension.
        /// </summary>
        TimeDimension,

        // METADATA STRUCTURE

        /// <summary>
        ///     The MSD.
        /// </summary>
        Msd,

        /// <summary>
        ///     The report structure.
        /// </summary>
        ReportStructure,

        /// <summary>
        ///     The metadata attribute.
        /// </summary>
        MetadataAttribute,

        // PROCESS

        /// <summary>
        ///     The process.
        /// </summary>
        Process,

        /// <summary>
        ///     The process step.
        /// </summary>
        ProcessStep,

        /// <summary>
        ///     The transition.
        /// </summary>
        Transition,

        // REGISTRY

        /// <summary>
        ///     The provision agreement.
        /// </summary>
        ProvisionAgreement,

        /// <summary>
        ///     The registration.
        /// </summary>
        Registration,

        /// <summary>
        ///     The subscription.
        /// </summary>
        Subscription,

        /// <summary>
        ///     The attachment constraint.
        /// </summary>
        AttachmentConstraint,

        /// <summary>
        ///     The content constraint.
        /// </summary>
        ContentConstraint,

        // MAPPING

        /// <summary>
        ///     The structure set.
        /// </summary>
        StructureSet,

        /// <summary>
        ///     The structure map.
        /// </summary>
        StructureMap,

        /// <summary>
        ///     The reporting taxonomy map.
        /// </summary>
        ReportingTaxonomyMap,

        /// <summary>
        ///     The representation map.
        /// </summary>
        RepresentationMap, // IMPORTANT WHAT IS THE OBJECT CLASS HERE?

        /// <summary>
        ///     The category map.
        /// </summary>
        CategoryMap,

        /// <summary>
        ///     The category scheme map.
        /// </summary>
        CategorySchemeMap,

        /// <summary>
        ///     The concept scheme map.
        /// </summary>
        ConceptSchemeMap,

        /// <summary>
        ///     The code map.
        /// </summary>
        CodeMap,

        /// <summary>
        ///     The code list map.
        /// </summary>
        CodeListMap,

        /// <summary>
        ///     The component map.
        /// </summary>
        ComponentMap,

        /// <summary>
        ///     The concept map.
        /// </summary>
        ConceptMap,

        /// <summary>
        ///     The organisation map.
        /// </summary>
        OrganisationMap,

        /// <summary>
        ///     The organisation scheme map.
        /// </summary>
        OrganisationSchemeMap,

        /// <summary>
        ///     The hybrid codelist map.
        /// </summary>
        HybridCodelistMap,

        /// <summary>
        ///     The hybrid code.
        /// </summary>
        HybridCode,

        /// <summary>
        ///     Target region
        /// </summary>
        MetadataTargetRegion,

        /// <summary>
        ///     The organisation.
        /// </summary>
        Organisation,

        /// <summary>
        ///     The organisation scheme.
        /// </summary>
        OrganisationScheme,

        /// <summary>
        ///     The primary measure.
        /// </summary>
        PrimaryMeasure
    }

}
