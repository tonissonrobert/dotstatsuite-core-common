﻿using System.Collections.Generic;
using log4net.Core;

namespace DotStat.Common.Configuration.Dto
{
    public class MySqlConfiguration {
        /// <summary>
        /// Set to true if columnstore indexes enabled for mysql database
        /// Mariadb - currently only Enterprise version of database support columnstore and needs to be configured in db before use.
        /// </summary>
        public bool UseColumnstoreIndexes { get; set; } = false;
    }
}
