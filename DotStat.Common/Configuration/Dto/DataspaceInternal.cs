﻿using System.Collections.Generic;
using log4net.Core;

namespace DotStat.Common.Configuration.Dto
{
    public class DataspaceInternal
    {
        /// <summary>
        /// Unique Id of dataspace (collect, process, disseminate, etc)
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Mapping store connection string
        /// </summary>
        public string DotStatSuiteCoreStructDbConnectionString { get; set; }
        /// <summary>
        /// DotStatSuiteCoreData database connection string
        /// </summary>
        public string DotStatSuiteCoreDataDbConnectionString { get; set; }
        /// <summary>
        /// DotStatSuiteCoreDataDbConnectionStringReadOnlyReplica database connection string to the readonly replica if any
        /// When this property is not set, the 'DotStatSuiteCoreDataDbConnectionString' is used. 
        /// Used for readonly database executions
        /// </summary>
        public string DotStatSuiteCoreDataDbConnectionStringReadOnlyReplica { get; set; }
        /// <summary>
        /// The timeout of data import/transfer transaction in minutes 
        /// </summary>
        public int DataImportTimeOutInMinutes { get; set; }
        /// <summary>
        /// The timeout in seconds applied on database command instances.
        /// </summary>
        public int DatabaseCommandTimeoutInSec { get; set; }
        /// <summary>
        /// Automatically generate log4net logging configuration to log into the Data DB
        /// </summary>
        public bool AutoLog2DB { get; set; } = false;
        /// <summary>
        /// Log level of the automatically generated log4net logger
        /// </summary>
        public string AutoLog2DBLogLevel { get; set; } = Level.Notice.Name;
        /// <summary>
        /// Whether to allow the use of the X-DotStat-Release header to target the PIT version of data.
        /// </summary>
        public bool AllowPITTargetDataVersion { get; set; } = true;
        /// <summary>
        /// Log level of the in-memory logger collecting log events to be sent to the user at the end of a transaction
        /// </summary>
        public string EmailLogLevel { get; set; } = Level.Notice.Name;
        /// <summary>
        /// The size of the batch of imported observations to log, if 0, no logs will be created for batches being loaded
        /// </summary>
        public int NotifyImportBatchSize { get; set; } = 500000;
        /// <summary>
        /// Notify that the transaction is still in progress every given minutes
        /// </summary>
        public int NotifyStillInProcessEveryMinutes { get; set; } = 5;
        
        /// <summary>
        /// Whether all the data and referential metadata of all DSDs in this dataspace should be compressed to save disk space.
        /// </summary>
        public bool Archive { get; set; } = false;
        
        /// <summary>
        /// Indicates what type of database connection should be used.
        /// The possible values of "DbType": "SqlServer", "MariaDb"
        /// Default: SqlServer
        /// </summary>
        public string DbType { get; set; } = "SqlServer";
        /// <summary>
        /// Indicates if this dataspace should keep history of the changes done to the data and referential metadata.
        /// </summary>
        public bool KeepHistory { get; set; } = false;
        
        /// <summary>
        /// Additional configurations for MySql based databases (e.g. MariaDb).
        /// </summary>
        public MySqlConfiguration MySql { get; set; }
    }
}
