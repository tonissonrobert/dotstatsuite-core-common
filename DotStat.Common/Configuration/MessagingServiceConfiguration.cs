﻿namespace DotStat.Common.Messaging
{
    public class MessagingServiceConfiguration
    {
        public const string MessageBrokerSettings = "messaging";

        /// <summary>
        /// Enable Servicebus/message queue 
        /// </summary>
        public bool Enabled { get; set; } = false;

        /// <summary>
        ///  Hostname of the RabbitMq instance, url or IP
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// Provide logical grouping and separation of resources
        /// </summary>
        public string VirtualHost { get; set; }

        public int Port { get; set; }

        public string QueueName { get; set; }

        public string Password { get; set; }

        public string UserName { get; set; }

        public bool QueueDurable { get; set; }

        public byte DeliveryMode { get; set; }
    }
}
