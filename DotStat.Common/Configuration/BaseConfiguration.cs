using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using log4net.Core;
using System.Collections.Generic;
using System.IO;

namespace DotStat.Common.Configuration
{
    /// <summary>
    /// This configuration should contain only really shared properties, from now on if configuration is specific only for 1 service/app it should define it's
    /// configuration in the same project as the main logic. Configuration is decentralized and every service/application can have it's own configuration loading mechanism 
    /// </summary>
    public class BaseConfiguration : IGeneralConfiguration, IMailConfiguration, IDataspaceConfiguration, ILocalizationConfiguration, ISecurityConfiguration
    {
        #region IGeneralConfiguration
        public int MaxTransferErrorAmount { get; set; }
        public string DefaultLanguageCode { get; set; }
        public string DotStatSuiteCoreCommonDbConnectionString { get; set; }
        public bool AutoLog2Stdout { get; set; } = false;
        public string AutoLog2StdoutLogLevel { get; set; } = Level.Debug.Name;
        public bool AutoLog2Google { get; set; }
        public string GoogleLogLevel { get; set; } = Level.Debug.Name;
        public string GoogleProjectId { get; set; }
        public string GoogleLogId { get; set; }
        public int MaxTextAttributeLength { get; set; } = 150;
        public int MaxTextDimensionLength { get; set; } = 150;
        public string TempFileDirectory { get; set; } = Path.Combine(Path.GetTempPath(), "TransferService");
        public int TempFileMaxAgeInHours { get; set; } = 24;
        public int MinPercentageDiskSpace { get; set; } = 25;
        public int HttpClientTimeOut { get; set; } = 120;
        public int MaxConcurrentTransactions { get; set; } = 10;

        #endregion

        #region IMailConfiguration
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public bool SmtpEnableSsl { get; set; }
        public string SmtpUserName { get; set; }
        public string SmtpUserPassword { get; set; }
        public string MailFrom { get; set; }
        public string SmtpHFrom { get; set; }
        #endregion

        #region IDataspaceConfiguration
        public IList<DataspaceInternal> SpacesInternal { get; set; }
        public bool ShowAdvanceDbHealthInfo { get; set; } = true;
        #endregion

        #region ILocalizationConfiguration
        public Dictionary<string, Dictionary<Localization.Localization.ResourceId, string>> LocalizedResources { get; set; }
        #endregion

        #region ISecurityConfiguration

        public string[] EddFileAllowedExtensions { get; set; } = { "xml" };
        public string[] ExcelFileAllowedExtensions { get; set; } = { "xlsx" };
        public string[] SDMXFileAllowedExtensions { get; set; } = { "csv", "xml", "zip" };

        public string[] EddFileAllowedContentTypes { get; set; } = 
        {
            "application/xml",
            "text/xml",
            "application/octet-stream"
        };
        public string[] ExcelFileAllowedContentTypes { get; set; } = 
        {
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xmld",
            "application/octet-stream"
        };
        public string[] SDMXFileAllowedContentTypes { get; set; } = 
        {
            "application/xml",
            "text/xml",
            "text/csv",
            "text/plain",
            "application/text",
            "application/x-zip-compressed",
            "application/zip",
            "application/octet-stream",
            "application/vnd.ms-excel"
        };
        public bool ApplyEmptyDataDbConnectionStringInMSDB { get; set; } = true;
        #endregion
    }
}
