﻿using System.Collections.Generic;

namespace DotStat.Common.Configuration.Interfaces
{
    public interface ISecurityConfiguration
    {
        #region File extensions
        string[] EddFileAllowedExtensions { get; set; }
        string[] ExcelFileAllowedExtensions { get; set; }
        string[] SDMXFileAllowedExtensions { get; set; }
        #endregion

        #region Mime types
        string[] EddFileAllowedContentTypes { get; set; }
        string[] ExcelFileAllowedContentTypes { get; set; }
        string[] SDMXFileAllowedContentTypes { get; set; }
        #endregion

        #region Password saving to db
        bool ApplyEmptyDataDbConnectionStringInMSDB { get; set; }
        #endregion
    }
}
