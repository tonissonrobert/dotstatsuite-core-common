﻿using System.Collections.Generic;
using DotStat.Common.Configuration.Interfaces;

namespace DotStat.Common.Configuration
{
    public class AuthConfiguration : IAuthConfiguration
    {
        /// <summary>
        /// Is openid authentication enabled
        /// </summary>
        public bool Enabled { get; set; }
        /// <summary>
        /// Authority url of token issuer
        /// </summary>
        public string Authority { get; set; }
        /// <summary>
        /// Client/application Id
        /// </summary>
        public string ClientId { get; set; }
        /// <summary>
        /// Authorization url (used in swagger UI interface)
        /// </summary>
        public string AuthorizationUrl { get; set; }
        /// <summary>
        /// Token url (used in swagger UI interface)
        /// </summary>
        public string TokenUrl { get; set; }
        /// <summary>
        /// Requested openId scopes (used as parameters for authorization url)
        /// </summary>
        public string[] Scopes { get; set; }
        /// <summary>
        /// Claims mapping rules
        /// </summary>
        public Dictionary<string, string> ClaimsMapping { get; set; }
        /// <summary>
        /// Is HTTPS connection to OpenId authority server required
        /// </summary>
        public bool RequireHttps { get; set; }
        /// <summary>
        /// Is iss (issuer) claim in JTW token should match configured authority
        /// </summary>
        public bool ValidateIssuer { get; set; }
        /// <summary>
        /// Expected aud (audience) claim  in JWT token, if not set equals to ClientId 
        /// </summary>
        public string Audience { get; set; }

        public static AuthConfiguration Default = new AuthConfiguration()
        {
            ClaimsMapping = new Dictionary<string, string>()
        };
    }
}
