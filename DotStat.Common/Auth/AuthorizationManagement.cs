﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Model;
using Estat.Sdmxsource.Extension.Constant;

namespace DotStat.Common.Auth
{
    /// <summary>
    /// Load and manage Users authorizations
    /// </summary>
    public partial class AuthorizationManagement : IAuthorizationManagement
    {
        private IAuthorizationRepository _repository;

        public AuthorizationManagement(IAuthorizationRepository repository)
        {
            _repository = repository;
        }

        public int SaveAuthorizationRule(UserAuthorization ua)
        {
            return ua.Id > 0
                ? _repository.Update(ua)
                : _repository.Insert(ua);
        }

        public bool DeleteAuthorizationRule(int id)
        {
            return _repository.Delete(id) > 0;
        }

        public bool IsAuthorized(DotStatPrincipal principal, PermissionType requestedPermission)
        {
            var rules = UserRules(principal).ToArray();

            if (!rules.Any())
                return false;

            return rules.Any(rule => rule.Permission.HasFlag(requestedPermission));
        }

        public bool IsAuthorized(DotStatPrincipal principal, string dataSpace, string artefactAgencyId, string artefactId, string artefactVersion, PermissionType requestedPermission)
        {
            var rules = UserRules(principal).ToArray();

            if (!rules.Any())
                return false;

            return rules.Any(rule => IsAuthorized(rule, dataSpace, artefactAgencyId, artefactId, artefactVersion, requestedPermission));
        }

        public IEnumerable<UserAuthorization> AllRules()
        {
            return _repository.GetAll();
        }

        public IEnumerable<UserAuthorization> UserRules(DotStatPrincipal principal)
        {
            return _repository.GetByUser(principal);
        }

        public UserAuthorization GetAuthorizationRule(int id)
        {
            return _repository.GetById(id);
        }

        public bool IsAuthorized(UserAuthorization rule, string dataSpace, string artefactAgencyId, string artefactId, string artefactVersion, PermissionType requestedPermission)
        {
            if (!IsMatch(rule.DataSpace, dataSpace))
                return false;

            if (!IsMatch(rule.ArtefactAgencyId, artefactAgencyId))
                return false;

            if (!IsMatch(rule.ArtefactId, artefactId))
                return false;

            if (!IsMatch(rule.ArtefactVersion, artefactVersion))
                return false;

            return rule.Permission.HasFlag(requestedPermission);
        }

        private static bool IsMatch(string field, string value)
        {
            return field == "*" || string.Compare(field, value, StringComparison.InvariantCultureIgnoreCase) == 0;
        }
    }
}