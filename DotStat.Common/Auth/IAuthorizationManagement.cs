﻿using System.Collections.Generic;
using DotStat.Common.Model;
using Estat.Sdmxsource.Extension.Constant;

namespace DotStat.Common.Auth
{
    public interface IAuthorizationManagement
    {
        /// <summary>
        /// Used when SDMX scope is not relevant
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="requestedPermission"></param>
        /// <returns></returns>
        bool IsAuthorized(DotStatPrincipal principal, PermissionType requestedPermission);

        /// <summary>
        /// Used when SDMX scope is relevant
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="dataSpace"></param>
        /// <param name="artefactAgencyId"></param>
        /// <param name="artefactId"></param>
        /// <param name="artefactVersion"></param>
        /// <param name="requestedPermission"></param>
        /// <returns></returns>
        bool IsAuthorized(DotStatPrincipal principal, string dataSpace, string artefactAgencyId, string artefactId, string artefactVersion, PermissionType requestedPermission);
        IEnumerable<UserAuthorization> AllRules();
        IEnumerable<UserAuthorization> UserRules(DotStatPrincipal principal);
        int SaveAuthorizationRule(UserAuthorization ua);
        bool DeleteAuthorizationRule(int id);
        UserAuthorization GetAuthorizationRule(int id);
    }
}