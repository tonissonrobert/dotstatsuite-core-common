﻿using log4net.Core;
using log4net.Filter;
using System;

namespace DotStat.Common.Logger
{
    public class ExactMatchPropertyFilter : FilterSkeleton
    {
        public string Key { get; set; }
        public string StringToMatch { get; set; }
        public bool AcceptOnMatch = true;

        public ExactMatchPropertyFilter()
        { 
        }

        public override FilterDecision Decide(LoggingEvent loggingEvent)
        {
            if (loggingEvent == null)
            {
                throw new ArgumentNullException(nameof(loggingEvent));
            }

            if (Key == null)
            {
                // We cannot filter so allow the filter chain
                // to continue processing
                return FilterDecision.Neutral;
            }

            // Lookup the parameter to match in from the properties 
            object msgObj = loggingEvent.LookupProperty(Key);

            // Use an ObjectRenderer to convert the property value to a string
            string msg = loggingEvent.Repository.RendererMap.FindAndRender(msgObj);

            // Check if we have been setup to filter
            if (msg == null || StringToMatch == null)
            {
                // We cannot filter so allow the filter chain
                // to continue processing
                return FilterDecision.Neutral;
            }

            if (msg.Equals(StringToMatch))
            {
                if (AcceptOnMatch)
                {
                    return FilterDecision.Accept;
                }

                return FilterDecision.Deny;
            }

            //Property value does not match, deny the logging event
            return FilterDecision.Deny;
        }
    }
}
