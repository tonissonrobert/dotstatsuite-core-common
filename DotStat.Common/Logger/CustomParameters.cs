﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.AspNetCore.Http;

namespace DotStat.Common.Logger
{
    /// <summary>
    /// Custom log parameter enum
    /// </summary>
    public enum CustomParameter
    {
        Application = 1,
        DataSpaceId = 2,
        TransactionId = 3,
        UserEmail = 4,
        MemberName = 5,
        FilePath = 6,
        LineNumber = 7,
        UrlReferrer = 8,
        HttpMethod = 9,
        RequestPath = 10,
        QueryString = 11,
    }

    /// <summary>
    /// Custom log parameter helper class
    /// </summary>
    public class CustomParameters
    {
        /// <summary>
        /// Custom parameters to be logged
        /// </summary>
        public ReadOnlyDictionary<CustomParameter, object> Parameters { get; private set; }

        /// <summary>
        /// Initializes the custom log parameters
        /// </summary>
        /// <param name="context"></param>
        public CustomParameters(HttpContext context)
        {
            Dictionary<CustomParameter, object> parameters = new Dictionary<CustomParameter, object>();

            if (context != null)
            {
                try
                {
                    parameters.Add(CustomParameter.HttpMethod, context.Request.Method);
                    parameters.Add(CustomParameter.RequestPath, context.Request.Path.Value);
                    parameters.Add(CustomParameter.QueryString, context.Request.QueryString.ToString());
                    if (!string.IsNullOrEmpty(context.Request.Headers["Referer"]))
                        parameters.Add(CustomParameter.UrlReferrer, context.Request.Headers["Referer"].ToString());
                }
                catch
                {
                    // DEVNOTE: swallow HttpException as when the method is called from Application_Start then the Request object is not yet available
                }
            }

            Parameters = new ReadOnlyDictionary<CustomParameter, object>(parameters);
        }
    }
}