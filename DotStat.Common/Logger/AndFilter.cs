﻿using log4net.Core;
using log4net.Filter;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DotStat.Common.Logger
{
    public class AndFilter : FilterSkeleton
    {
        private readonly IList<IFilter> _filters = new List<IFilter>();

        public override FilterDecision Decide(LoggingEvent loggingEvent)
        {
            if (loggingEvent == null)
                throw new ArgumentNullException(nameof(loggingEvent));

            if (_filters.Any(filter => filter.Decide(loggingEvent) != FilterDecision.Accept))
            {
                return FilterDecision.Neutral; // one of the filter has failed
            }

            // All conditions are true
            return AcceptOnMatch ? FilterDecision.Accept : FilterDecision.Deny;
        }

        public void AddFilter(IFilter filter)
        {
            _filters.Add(filter);
        }

        public IFilter Filter
        {
            set => _filters.Add(value);
        }

        public bool AcceptOnMatch { get; set; }
    }
}