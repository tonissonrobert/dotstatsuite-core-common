namespace DotStat.Common.Localization
{
    public class Localization
    {
        public enum ResourceId
        {
            MailSent,
            MailSubject,
            MailSuccessfullyCompleted,
            MailCompletedWithWarnings,
            MailFailed,
            MailBody,
            EmailSummary,
            EmailSummarySourceDataSpace,
            EmailSummaryDataSourceTransactionQuery,
            EmailSummaryDataSourceTransactionNoQuery,
            EmailSummaryDataSourceEddExcel,
            DeleteAllActionPerformed,
            DataObservationsProcessedDetails,
            DataObservationsProcessedDetailsObsLevel,
            DataObservationsProcessedDetailsSeriesLevel,
            DataObservationsProcessedDetailsDataFlowLevel,
            DataObservationsProcessedNoDetails,
            MetaDataObservationsProcessedDetails,
            MetaDataObservationsProcessedNoDetails,
            ReadingSourceFinished,
            ReadingMetaSourceFinished,
            StillProcessing,
            StartedReadingAndValidating,
            StartedReadingAndValidatingMetadata,
            StartedValidating,
            StartedValidatingMetadata,
            StartedWritingData,
            StartedWritingDataMetadata,
            StartedCalculatingAvailability,
            ProcessingObservations,
            ProcessingMetadataAttributes,
            NoObservationsProcessed,
            UnhandledExceptionOccurred,
            SubmissionResult,
            DataflowLoaded,
            DsdLoaded,
            StreamingFinished,
            PluginProcessFinished,
            TransactionAbortedDueToConcurrentTransaction,
            TransactionAbortedDueToConcurrentTransactionWithNoDsd,
            PreviousTransactionTimedOut,
            PreviousTransactionWithNoDsdTimedOut,
            TransactionSuccessful,
            ValidationSucceeded,
            NoActiveTransactionWithId,
            TerminatingTimedOutTransaction,
            TerminatingCanceledTransaction,

            CodeNotFoundForDimension,
            DimensionNotFoundByCodeListTranslator,
            DataflowNotFoundInDataspace,
            DsdNotFoundInDataspace,
            DimensionDoesNotBelongToDataset,
            AttributeDoesNotBelongToDataset,
            DuplicateDsdComponentIds,
            DuplicateDsdMsdComponentIds,

            MissingCodeMember,
            UnknownCodeMember,
            DimensionConstraintViolation,
            AttributeConstraintViolation,
            NullValue,
            DuplicatedCoordinate, 
            DuplicatedRowsInStagingTable,
            OverlappingCoordinates,
            MultipleRowsModifyingTheSameRegionInStagingTable,

            TransactionAbortedDueToConcurrentArtefactAccess,
            ChangeInDsdCommonMessage,
            ChangeInMsdCommonMessage,
            ChangeInDsdDimensionToTimeDimension,
            ChangeInDsdDimensionAdded,
            ChangeInDsdDimensionRemoved,
            ChangeInDsdDimensionCodelistChanged,
            ChangeInDsdDimensionCodelistCodeRemoved,
            ChangeInDsdDimensionCodelistCodeAdded,
            ChangeInDsdTimeDimensionAdded,
            ChangeInDsdTimeDimensionRemoved,
            ChangeInDsdAttributeAdded,
            ChangeInDsdAttributeCodedRepresentationChanged,
            ChangeInDsdAttributeMandatoryStateChanged,
            ChangeInDsdAttributeCodelistChanged,
            ChangeInDsdAttributeRemoved,
            ChangeInDsdAttributeCodelistCodeRemoved,
            ChangeInDsdAttributeCodelistCodeAdded,
            ChangeInDsdAttributeAttachmentLevelChanged,
            ChangeInDsdAttributeAttachmentGroupChanged,
            ChangeInDsdAttributeTextFormatChanged,
            ChangeInDsdMeasureAdded,
            ChangeInDsdMeasureRemoved,
            ChangeInDsdMeasureCodedRepresentationChanged,
            ChangeInDsdMeasureCodelistChanged,
            ChangeInDsdMeasureCodelistCodeRemoved,
            ChangeInDsdMeasureCodelistCodeAdded,
            ChangeInDsdMeasureTextFormatChanged,
            ChangeInDsdMeasurePatternFacetChanged,
            ChangeInDsdMeasureMinLengthFacetChanged,
            ChangeInDsdMeasureMaxLengthFacetChanged,
            ChangeInDsdMeasureMinValueFacetChanged,
            ChangeInDsdMeasureMaxValueFacetChanged,
            ChangeInDsdMsdAdded,
            ChangeInDsdMsdChanged,
            ChangeInDsdMsdRemoved,

            ChangeInMsdAttributeAdded,
            ChangeInMsdAttributeCodedRepresentationChanged,
            ChangeInMsdAttributeRemoved,
            
            InvalidCodeInAllowedContentConstraint,
			
            FailedToOpenDatastoreDbConnectionForCleanup,
            FailedToOpenDatastoreDbConnection,
            DatabaseTableNotFound,
            AttributeNotDefinedInDsd,
            AttributeReportedAtMultipleLevels,

            MandatoryDatasetAttributeWithNullValueInDatabase,
            MandatoryAttributeWithNullValueInDatabase,
            MandatoryDatasetAttributeWithNullValueInStaging,
            MandatoryDimGroupAttributeWithNullValueInStaging,
            MandatoryObservationAttributeWithNullValueInStaging,
            MultipleValuesForDatasetAttribute,
            MultipleValuesForDimGroupAttributeInDatabase,
            MultipleValuesForDimGroupAttributeInStaging,
            AttributeReferencedDimensionMissing,
            ObservationReferencedDimensionMissing,

            UnknownAttributeCodeMemberWithoutCoordinate,
            UnknownAttributeCodeMemberWithCoordinate,
            DuplicatedDatasetAttribute,
            DuplicatedAttributeKey,
            WrongSetOfDimensionsInKeyOfKeyable,
            MissingCodeMemberWithCoordinate,
            UnknownCodeMemberWithCoordinate,
            UnknownCodeMemberWithoutCoordinate,
            ObservationAttributeReportedAtDataset,
            ObservationAttributeReportedAtKeyables,
            DatasetAttributeReportedAtKeyables,
            DimGroupAttributeReportedAtDataset,
            MandatoryDatasetAttributeMissing,
            MandatoryDimGroupAttributeMissing,
            MandatoryObservationAttributeMissing,
            TimeAtTimeDimensionNotSupported,
            TimeDimensionFormatNotRecognized,

            TextAttributeValueLengthExceedsMaxLimitWithCoordinate,
            TextAttributeValueLengthExceedsMaxLimitWithoutCoordinate,
            UnknownAttributeAttachmentLevel,
            MsdInvalidDbId,
            AttributeInvalidDbId,
            MetadataAttributeInvalidDbId,
            CodelistInvalidDbId,
            DimensionInvalidDbId,
            MeasureInvalidDbId,
            NoDSDMappedComponent,
            MissingDsdForAttribute,
            MissingGroupForAttribute,

            SDMXMLErrorGropsAfterSeries,

            NotASqlManagementRepository,
            CopyWithMultipleDbCredentials,
            UnknownComponentType, 
            NotMetadataAttributeComponentType,
            ComponentNotFound,
            MetadataAttributeNotFound,
            DsdNotInitialized,
            ArtefactTypeNotRecognized,
            NoDimensionFound,
            NoDimensionFoundInManagementDb,
            IncorrectArtefactType,
            DsdNotSet,
            ArtefactNotFoundInManagementDb,
            EmptyCodelist,
            DsdNotFound,
            MsdNotFound,
            DataflowParameterWithoutDsd,
            NoLiveOrPITVersion,
            NonExistingPITVersion,
            NonExistingLiveVersion,
            PITReleased,
            PITNotReleased,
            NoRestoringVersion,
            MismatchTargetVersion,
            CodelistNotDefined,
            WrongSdmxVersionFormat,
            NotASdmxVersionObject,
            DSDNotFoundBySdmxParser,
            DataflowNotFoundBySdmxParser,
            NoCodelistRepresentation,

            SdmxSourceHttpRequestException,

            MaximumTextAttributeLengthDecreaseNotSupported,
            MaximumTextAttributeLengthNewValueApplied,
            MaximumTextAttributeLengthInvalidDSDAnnotationValue,

            CleanUpErrorDsdStillExists,
            CleanUpErrorNothingToDelete,
            CleanUpErrorConcurrentTransaction,
            CleanUpErrorConcurrentTransactionV2,
            CleanUpSuccessful,
            CleanUpSuccessfulCodelist,
            NoOrphanDsdsToClean,

            DataSpaceNotProvided,
            DataSpaceNotFound,
            NoTimeZoneProvidedForUpdatedAfter,

            DSDNotProvided,
            WrongDSDFormat,

            MSDNotProvided,
            WrongMSDFormat,

            MissingMsdReference,
            MSDNotFoundInStrucDB,

            DataFlowNotProvided,
            WrongDataFlowFormat,
            NoDataflowReferenceInputDataset,

            WrongPITReleaseDateFormat,
            NoValueSetForDsdLiveVersion,
            TargetVersionInDBChanged,

            FileAttachmentNotProvided,
            FileNotFound,
            NotValidFileExtension,
            ContentTypesUndefined,
            NotValidContentType,
            NotValidFileSignature,
            JsonFormatNotSupported,
            TemporaryFileDirectoryFull,
            TemporaryFileDirectoryNotAccessible,
            TemporaryFileDirectoryNotValid,
            DeleteTempFileFailBackgroundTask,
            DeleteTempFileFailImport,
            BoundaryLengthLimit,
            MissingContentTypeBoundary,
            WrongContentType,
            RequestBodyKeyLimit,
            HttpClientTimeOut,

            InitDBObjectsOfDataflowSuccess,
            InitDBObjectsOfDataflowFail,
            InitDBObjectsOfDataflowAlreadyDone,
            InitDBObjectsOfDataflowConcurrentTransaction,

            CsvInvalidDelimiter,
            CsvMissingDimensions,
            NotIncludedComponents,
            SourceFileHasNoComponentsToProcess,

            ExcelMapperFieldNotFound,
            ExcelMapperSourceMappingNotConsistent,
            ExcelMapperTargetMappingNotConsistent,
            ExcelMapperInvalidOrDuplicate,
            ExcelMapperUnmatchingName,
            ExcelMapperDuplicateColumnsInSource,
            ExcelMapperDuplicateColumnsInTarget,
            ExcelMapperSourceNotConsistentWithDefinition,
            ExcelMapperTargetNotConsistentWithDefinition,
            ExcelMapperSourceEmptyColumns,
            ExcelMapperTargetEmptyColumns,
            ExcelMapperTargetTemplateSize,
            V8MapperWrongInputArray,
            V8MapperNullAttribute,
            StandardSWMapperMapAlreadyDefined,
            StandardSWMapperDimensionNotDefinedTarget,
            StandardSWMapperDimensionNotDefinedSource,
            StandardSWMapperDimensionsAlreadyDefined,
            StandardSWMapperUnmatchingDimension,
            StandardMapperFieldNotInSource,
            StandardMapperFieldNotInDataflow,
            ExcelReaderInvalidCellReferenceInSource,
            ExcelReaderInvalidCellExclusion,
            ExcelReaderCellNotInSheet,
            ExcelReaderInvalidCellReference,
            ExcelWorkbookNotLocated,
            ExcelNoWorkSheets,
            ExcelCellNotLocated,
            ExcelSheetNotLocated,
            ExcelNullDatasetAttribute,
            ExcelUnknownIndex,
            ExcelInvalidDataValue,
            ExcelDataSourceNotSet,
            ExcelNoCoordinateTranslators,
            ExcelMultipleCoordinateMappingDescriptions,
            ExcelWorksheetRegex,
            ExcelExpressionParcingError,
            ExcelNoLeftCell,
            ExcelNoAboveCell,
            ExcelRowPositiveValue,
            ExcelColumnPositiveValue,
            ExcelColumnLetterValue,
            ExcelCellNotation,
            ExcelFindFirstCellNoCellWithRangePattern,
            ExcelFindLastCellNoCellWithRangePattern,
            ExcelFirstNonEmptyCell,
            ExcelLastNonEmptyCell,
            ExcelWrongNameAndCondition,
            ExcelMappingNameNoProvided,
            ExcelExternalMemberNoSpecified,
            ExcelWrongNumberOfCoordinates,
            ExcelNonmatchingFixedMemberAttribute,
            ExcelValueNotInExternField,
            ExcelValueNotInSourceField,
            ExcelNoData,
            ExcelReaderResetNotSupported,
            ExcelUnexpectedError,
            ExcelIndexNameNotAvailable,
            ExcelIndexNameNotFound,
            ExcelIndexerNotLoaded,
            ExcelIterationNotStarted,

            EddExpressionEvaluation,
            EddValueOrCoordinateExpression,
            EddAxisRefNotSpecified,
            EddCannotParseAxis,
            EddIntegerExpression,
            EddStringCollectionExpression,
            EddStringExpression,
            EddEmptyCellReference,
            EddFirstActiveCellAsOwner,
            EddInvalidCellReference,
            EddBreakdownNonEmpty,
            EddParceError,
            EddSpecifyCode,
            EddFileNotLoaded,

            EmailParameterError,

            DataFlowForDisseminationError,
            CleanupMappingSetSuccess,
            CleanUpNoMappingSet,
            CleanUpErrorMappingSetConcurrentTransaction,
            CleanUpError,
            CleanUpErrorCodelist,
            CleanUpErrorV2,
            CleanUpErrorUnauthorized,
            CleanUpErrorUnauthorizedCodelist,

            MaintenanceTransactionFailedOngoingTransactions,
            OngoingTransactionBlockingAll,

            NoDsdsInMappingStoreDb,
            NoDataflowsInMappingStoreDb,
            CreationOfMappingsetsSuccessForDataflows,
            UpdatedMappingsetsOfDataflows,
            MappingsetsPreviouslyCreatedForDataflows,
            CreationOfMappingsetsFailedForDataflows,
            CreateAllMappingsetsCompleted,
            CreateAllMappingsetsFailed,
            ErrorTryingToCreateMappingSet,
            ErrorTryingToCreateDataSet,
            ErrorTryingToCreateConnection,
            ErrorTryingToDeleteMappingSet,
            ErrorTryingToDeleteDataSet,
            ErrorTryingToUpdateMappingSet,
            UnexpectedErrorWithDataflow,
            InitExternalDataflow,
            ErrorTryingToCreateActualContentConstraint,

            UnauthorizedImport,
            UnauthorizedToMergeCsv,
            UnauthorizedToMergeXml,
            UnauthorizedToDeleteCsv,
            UnauthorizedToDeleteXml,
            ActionNotSupported,

            WrongFormat,
            EndDateBeforeStart,

            ExternalDataflow,

            DatabaseVersionNotSupported,
            DatabaseVersionIsInvalid,

            InvalidValueFormatNotAlpha,
            InvalidValueFormatNotAlphaNumeric,
            InvalidValueFormatNotNumeric,
            InvalidValueFormatNotBigInteger,
            InvalidValueFormatNotInteger,
            InvalidValueFormatNotLong,
            InvalidValueFormatNotShort,
            InvalidValueFormatNotDecimal,
            InvalidValueFormatNotFloat,
            InvalidValueFormatNotDouble,
            InvalidValueFormatNotBoolean,
            InvalidValueFormatNotUri,
            InvalidValueFormatNotCount,
            InvalidValueFormatNotObservationalTimePeriod,
            InvalidValueFormatNotStandardTimePeriod,
            InvalidValueFormatNotBasicTimePeriod,
            InvalidValueFormatNotGregorianTimePeriod,
            InvalidValueFormatNotGregorianYear,
            InvalidValueFormatNotGregorianYearMonth,
            InvalidValueFormatNotGregorianDay,
            InvalidValueFormatNotReportingTimePeriod,
            InvalidValueFormatNotReportingYear,
            InvalidValueFormatNotReportingSemester,
            InvalidValueFormatNotReportingTrimester,
            InvalidValueFormatNotReportingQuarter,
            InvalidValueFormatNotReportingMonth,
            InvalidValueFormatNotReportingWeek,
            InvalidValueFormatNotReportingDay,
            InvalidValueFormatNotDateTime,
            InvalidValueFormatNotTimeRange,
            InvalidValueFormatNotMonth,
            InvalidValueFormatNotMonthDay,
            InvalidValueFormatNotDay,
            InvalidValueFormatNotTime,
            InvalidValueFormatNotDuration,
            InvalidValueFormatPatternMismatch,
            InvalidValueFormatMaxLength,
            InvalidValueFormatMinLength,
            InvalidValueFormatMaxValue,
            InvalidValueFormatMinValue,
            InvalidValueFormatUnknownCodeMember,
            InvalidValueFormatNotSupported,
            NoMeasureRepresentationWarning,

            MissingDimensionMetadataImport,
            DeleteAllMetadataActionPerformed,
            NonUniqueComponentIdAcrossDsdAndMsd,

            StartCompressingDsd,
            FinishCompressingDsd,
            ErrorCompressingDsd,

            PITAndKeepHistoryNotSupportedError,
            DsdReferenceNotSupported,
            DsdReferenceNotSupportedExcel

        }
    }
}
