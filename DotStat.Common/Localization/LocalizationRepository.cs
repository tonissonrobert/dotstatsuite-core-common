﻿using System;
using System.Collections.Generic;
using System.Threading;
using DotStat.Common.Configuration.Interfaces;

namespace DotStat.Common.Localization
{
    public static class LocalizationRepository
    {
        private static Dictionary<string, Dictionary<Localization.ResourceId, string>>  _localizedResources { get; set; }

        public static void Configure(ILocalizationConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            
            _localizedResources = configuration.LocalizedResources;
        }

        public static string GetLocalisedResource(Localization.ResourceId resourceId, string languageCode)
        {
            if (string.IsNullOrEmpty(languageCode))
            {
                throw new ArgumentNullException(nameof(languageCode));
            }
            
            var localisedResource = GetResource(resourceId, languageCode) ??
                                    GetResource(resourceId, "en");

            if (string.IsNullOrEmpty(localisedResource))
            {
                throw new InvalidOperationException($"The localised resource '{resourceId}' was not found for languageCode '{languageCode}'");
            }

            return localisedResource;
        }

        public static string GetLocalisedResource(Localization.ResourceId resourceId)
        {
            var languageCode = Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;

            return GetLocalisedResource(resourceId, languageCode);
        }

        private static string GetResource(Localization.ResourceId resourceId, string languageCode)
        {
            if (!_localizedResources.ContainsKey(languageCode))
                return null;

            return _localizedResources[languageCode].ContainsKey(resourceId) ?
                   _localizedResources[languageCode][resourceId] : null;

        }
    }
}
