﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Logger;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Filter;
using log4net.Repository.Hierarchy;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;

namespace DotStat.Common.Test
{
    [TestFixture]
    internal class LoggerTests
    {
        [Test]
        public void SetupHttpContext()
        {
            const int transactionId = 1;
            const string userEmail = "user@email.com";
            const string dataspaceIdDesign = "design";
            const string httpMethod = "GET";
            const string path = "/index.html";
            const string queryString = "";
            const string referer = "http://localhost";

            Log.Configure(null);

            var dataspace = new DataspaceInternal()
            {
                Id = dataspaceIdDesign,
                EmailLogLevel = "NOTICE"
            };

            LogHelper.RecordNewTransaction(transactionId, dataspace);
            var memoryAppender = new MemoryAppender();
            BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetEntryAssembly()), memoryAppender);
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.AddAppender(memoryAppender);

            Log.SetDataspaceId(dataspaceIdDesign);
            Log.SetTransactionId(transactionId);
            Log.SetUserEmail(userEmail);

            const string notLoggedMsg = "not logging Msg";
            Assert.Throws<NullReferenceException>(() => Log.Info(notLoggedMsg));

            var contextAccessor = SetupHttpContextAccessor(httpMethod, path, queryString, referer).Object;
            Log.Configure(contextAccessor);
            const string loggedMsg = "logged Msg";
            Log.Info(loggedMsg);

            var loggedEvents = memoryAppender.GetEvents();
            hierarchy.Root.RemoveAppender(memoryAppender);

            Assert.AreEqual(1, loggedEvents.Length);
            Assert.AreEqual(loggedMsg, loggedEvents[0].MessageObject);
            Assert.AreEqual(userEmail, loggedEvents[0].Properties[CustomParameter.UserEmail.ToString()]);
        }

        [Test]
        public void LogLevelsTest()
        {
            var memoryAppender = new MemoryAppender();
            BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetEntryAssembly()), memoryAppender);
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.AddAppender(memoryAppender);

            Log.Configure(SetupHttpContextAccessor().Object);

            var logs = new Dictionary<Level, string>
            {
                { Level.Debug, "debug msg" },
                { Level.Info, "info msg" },
                { Level.Warn, "warn msg" },
                { Level.Error, "error msg" },
                { Level.Fatal, "fatal msg" },
                { Level.Notice, "notice msg" }
            };

            Log.Debug(logs[Level.Debug]);
            Log.Info(logs[Level.Info]);
            Log.Warn(logs[Level.Warn]);
            Log.Error(new Exception(logs[Level.Error]));
            Log.Fatal(new Exception(logs[Level.Fatal]));
            Log.Notice(logs[Level.Notice]);
            Log.Error(logs[Level.Error]);

            var loggedEvents = memoryAppender.GetEvents();

            Assert.AreEqual(7,loggedEvents.Length);

            Assert.AreEqual(logs[Level.Debug],loggedEvents[0].RenderedMessage);
            Assert.AreEqual(Level.Debug, loggedEvents[0].Level);

            Assert.AreEqual(logs[Level.Info], loggedEvents[1].RenderedMessage);
            Assert.AreEqual(Level.Info, loggedEvents[1].Level);

            Assert.AreEqual(logs[Level.Warn], loggedEvents[2].RenderedMessage);
            Assert.AreEqual(Level.Warn, loggedEvents[2].Level);

            Assert.AreEqual(logs[Level.Error], loggedEvents[3].RenderedMessage);
            Assert.AreEqual(Level.Error, loggedEvents[3].Level);

            Assert.AreEqual(logs[Level.Fatal], loggedEvents[4].RenderedMessage);
            Assert.AreEqual(Level.Fatal, loggedEvents[4].Level);

            Assert.AreEqual(logs[Level.Notice], loggedEvents[5].RenderedMessage);
            Assert.AreEqual(Level.Notice, loggedEvents[5].Level);
            
            Assert.AreEqual(logs[Level.Error], loggedEvents[6].RenderedMessage);
            Assert.AreEqual(Level.Error, loggedEvents[6].Level);

            hierarchy.Root.RemoveAppender(memoryAppender);
        }

        [Test]
        public void RetrieveCustomParameters()
        {
            const string httpMethod = "GET";
            const string path = "/index.html";
            const string queryString = "";
            const string referer = "http://localhost";

            var context = SetupHttpContextAccessor(httpMethod, path, queryString, referer).Object.HttpContext;
            var parameters = new CustomParameters(context).Parameters;

            Assert.AreEqual(httpMethod,parameters[CustomParameter.HttpMethod]);
            Assert.AreEqual(path, parameters[CustomParameter.RequestPath]);
            Assert.AreEqual(queryString, parameters[CustomParameter.QueryString]);
            Assert.AreEqual(referer, parameters[CustomParameter.UrlReferrer]);
        }

        [Test]
        public void AutomaticallyAddDataspaceAppenders()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.RemoveAllAppenders();

            const string designConnectionString = "designConnectionString";
            var design = new DataspaceInternal()
                { Id = "design", DotStatSuiteCoreDataDbConnectionString = designConnectionString, AutoLog2DB = true, AutoLog2DBLogLevel = "deBUG"};

            const string stagingConnectionString = "stagingConnectionString";
            var staging = new DataspaceInternal()
                { Id = "staging", DotStatSuiteCoreDataDbConnectionString = stagingConnectionString, AutoLog2DB = true };

            const string disseminateConnectionString = "disseminateConnectionString";
            var disseminate = new DataspaceInternal()
                { Id = "disseminate", DotStatSuiteCoreDataDbConnectionString = disseminateConnectionString , AutoLog2DB = false };

            var dataSpaces = new List<DataspaceInternal>
            {
                design,
                staging,
                disseminate
            };
            var config = new BaseConfiguration(){SpacesInternal = dataSpaces};

            LogHelper.ConfigureAppenders(config);
            
            var designAppender = (AdoNetAppender)hierarchy.Root.GetAppender("design");
            var stagingAppender = (AdoNetAppender)hierarchy.Root.GetAppender("staging");
            var disseminateAppender = (AdoNetAppender)hierarchy.Root.GetAppender("disseminate");
            
            Assert.NotNull(designAppender);
            Assert.AreEqual(designConnectionString, designAppender.ConnectionString);
            Assert.AreEqual(designAppender.Threshold, Level.Debug);

            Assert.NotNull(stagingAppender);
            Assert.AreEqual(stagingConnectionString, stagingAppender.ConnectionString);
            Assert.AreEqual(stagingAppender.Threshold, Level.Notice);

            Assert.IsNull(disseminateAppender);

            hierarchy.Root.RemoveAllAppenders();
        }
        
        [Test]
        public void AutomaticallyAddConsoleAppender()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.RemoveAllAppenders();
            
            var config = new BaseConfiguration() { AutoLog2Stdout = true};

            LogHelper.ConfigureAppenders(config);

            var standardOutputAppender = (ConsoleAppender)hierarchy.Root.GetAppender("StandardOutputAppender");

            Assert.NotNull(standardOutputAppender);

            Assert.AreEqual(standardOutputAppender.Threshold, Level.Debug);
            
            hierarchy.Root.RemoveAllAppenders();

            config.AutoLog2Stdout = false;
            LogHelper.ConfigureAppenders(config);

            standardOutputAppender = (ConsoleAppender)hierarchy.Root.GetAppender("StandardOutputAppender");

            Assert.Null(standardOutputAppender);
        }

        [Test]
        public void SystemAppenderFilters()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.RemoveAllAppenders();

            var memoryAppender = new MemoryAppender
            {
                Name = "SystemActivityAppender"
            };

            BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetEntryAssembly()), memoryAppender);
            hierarchy.Root.AddAppender(memoryAppender);
            
            var design = new DataspaceInternal(){ Id = "design", AutoLog2DB = true };
            var dataSpaces = new List<DataspaceInternal>{ design };

            var config = new BaseConfiguration() { SpacesInternal = dataSpaces };
            LogHelper.ConfigureAppenders(config);
            
            var filter = memoryAppender.FilterHead;
            Assert.AreEqual(CustomParameter.HttpMethod.ToString(), ((PropertyFilter)filter).Key);
            Assert.AreEqual("GET", ((PropertyFilter)filter).StringToMatch);
            Assert.AreEqual(false, ((PropertyFilter)filter).AcceptOnMatch);

            filter = filter.Next;
            Assert.AreEqual(CustomParameter.HttpMethod.ToString(), ((PropertyFilter)filter).Key);
            Assert.AreEqual("POST", ((PropertyFilter)filter).StringToMatch);
            Assert.AreEqual(false, ((PropertyFilter)filter).AcceptOnMatch);
            
            filter = filter.Next;
            Assert.AreEqual("Org.Sdmxsource", ((LoggerMatchFilter)filter).LoggerToMatch);
            Assert.AreEqual(false, ((LoggerMatchFilter)filter).AcceptOnMatch);

            filter = filter.Next;
            Assert.AreEqual("Estat.Sdmxsource", ((LoggerMatchFilter)filter).LoggerToMatch);
            Assert.AreEqual(false, ((LoggerMatchFilter)filter).AcceptOnMatch);

            filter = filter.Next;
            Assert.AreEqual("Estat.Sri.MappingStoreRetrieval", ((LoggerMatchFilter)filter).LoggerToMatch);
            Assert.AreEqual(false, ((LoggerMatchFilter)filter).AcceptOnMatch);

            filter = filter.Next;
            Assert.AreEqual("org.estat.sri.sqlquerylogger", ((LoggerMatchFilter)filter).LoggerToMatch);
            Assert.AreEqual(false, ((LoggerMatchFilter)filter).AcceptOnMatch);
            
            filter = filter.Next;
            Assert.AreEqual(CustomParameter.DataSpaceId.ToString(), ((ExactMatchPropertyFilter)filter).Key);
            Assert.AreEqual(design.Id, ((ExactMatchPropertyFilter)filter).StringToMatch);
            Assert.AreEqual(false, ((ExactMatchPropertyFilter)filter).AcceptOnMatch);

            Assert.AreEqual(null,filter.Next);

            hierarchy.Root.RemoveAllAppenders();
        }

        [Test]
        public void AllActivityAppenderFilters()
        {
            var memoryAppender = new MemoryAppender
            {
                Name = "AllActivityAppender"
            };

            BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetEntryAssembly()), memoryAppender);
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.RemoveAllAppenders();
            hierarchy.Root.AddAppender(memoryAppender);

            var design = new DataspaceInternal(){ Id = "design", AutoLog2DB = true };
            var dataSpaces = new List<DataspaceInternal>{design};

            var config = new BaseConfiguration() { SpacesInternal = dataSpaces };
            LogHelper.ConfigureAppenders(config);

            var filter = memoryAppender.FilterHead;
            Assert.AreEqual(CustomParameter.DataSpaceId.ToString(), ((ExactMatchPropertyFilter)filter).Key);
            Assert.AreEqual(design.Id, ((ExactMatchPropertyFilter)filter).StringToMatch);
            Assert.AreEqual(false, ((ExactMatchPropertyFilter)filter).AcceptOnMatch);

            Assert.AreEqual(null, filter.Next);

            hierarchy.Root.RemoveAllAppenders();
        }

        [Test]
        public void UserActivityAppenderFilters()
        {
            var memoryAppender = new MemoryAppender
            {
                Name = "UserActivityAppender"
            };

            BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetEntryAssembly()), memoryAppender);
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.RemoveAllAppenders();
            hierarchy.Root.AddAppender(memoryAppender);

            var design = new DataspaceInternal(){ Id = "design", AutoLog2DB = true };
            var dataSpaces = new List<DataspaceInternal>{design};

            var config = new BaseConfiguration() { SpacesInternal = dataSpaces };
            LogHelper.ConfigureAppenders(config);

            var filter = memoryAppender.FilterHead;
            Assert.AreEqual(CustomParameter.DataSpaceId.ToString(), ((ExactMatchPropertyFilter)filter).Key);
            Assert.AreEqual(design.Id, ((ExactMatchPropertyFilter)filter).StringToMatch);
            Assert.AreEqual(false, ((ExactMatchPropertyFilter)filter).AcceptOnMatch);

            Assert.AreEqual(null, filter.Next);

            hierarchy.Root.RemoveAllAppenders();
        }

        [Test]
        public void RecordNewTransaction()
        {
            var transactionId = 1;
            const string dataspaceIdDesign = "design";

            var userMessageDesign = $"user msg transaction_Id={transactionId}, dataspace={dataspaceIdDesign}";

            var dataspace = new DataspaceInternal()
            {
                Id = dataspaceIdDesign
            };

            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.RemoveAllAppenders();

            // No logs should be recorded, until transactionId and DataspaceId have been set
            LogHelper.RecordNewTransaction(transactionId, dataspace);

            var designAppender = hierarchy.Root.GetAppender($"{transactionId}_{dataspaceIdDesign}");
            BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetEntryAssembly()),designAppender);
            Log.Configure(SetupHttpContextAccessor().Object);
            
            Assert.NotNull(designAppender);
            
            Log.Debug(userMessageDesign);
            Log.Info(userMessageDesign);

            var designLogEvents = ((MemoryAppender)designAppender).GetEvents();
            Assert.AreEqual(0, designLogEvents.Length);

            // Appender should not be released after the logs have been read
            designAppender = hierarchy.Root.GetAppender($"{transactionId}_{dataspaceIdDesign}");
            Assert.IsNotNull(designAppender);

            // Logs should be recorded for the given transactionId and dataspaceId
            transactionId = 2;
            Log.SetTransactionId(transactionId);
            Log.SetDataspaceId(dataspaceIdDesign);

            LogHelper.RecordNewTransaction(transactionId, dataspace);

            designAppender = hierarchy.Root.GetAppender($"{transactionId}_{dataspaceIdDesign}");
      
            Assert.NotNull(designAppender);
            Assert.AreEqual($"{transactionId}_{dataspaceIdDesign}",designAppender.Name);

            Log.Debug(userMessageDesign);
            Log.Info(userMessageDesign+"Info");
            Log.Notice(userMessageDesign + "Notice");
            Log.Warn(userMessageDesign + "Warn");
            Log.Error(new Exception(userMessageDesign + "Error"));
            Log.Fatal(new Exception(userMessageDesign + "Fatal"));

            designLogEvents = ((MemoryAppender) designAppender).GetEvents();  

            //Debug and Info messages should not be logged
            Assert.AreEqual(4, designLogEvents.Length);

            Assert.AreEqual(userMessageDesign + "Notice", designLogEvents[0].RenderedMessage);
            Assert.AreEqual(Level.Notice, designLogEvents[0].Level);

            Assert.AreEqual(userMessageDesign + "Warn", designLogEvents[1].RenderedMessage);
            Assert.AreEqual(Level.Warn, designLogEvents[1].Level);

            Assert.AreEqual(userMessageDesign + "Error", designLogEvents[2].RenderedMessage);
            Assert.AreEqual(Level.Error, designLogEvents[2].Level);

            Assert.AreEqual(userMessageDesign + "Fatal", designLogEvents[3].RenderedMessage);
            Assert.AreEqual(Level.Fatal, designLogEvents[3].Level);
            
            // Appender should not be released after the logs have been read
            designAppender = hierarchy.Root.GetAppender($"{transactionId}_{dataspaceIdDesign}");
            Assert.IsNotNull(designAppender);
            
            hierarchy.Root.RemoveAllAppenders();
        }

        [Test]
        public void GetRecordedEvents()
        {
            const int transactionId = 1;
            const string dataspaceIdDesign = "design";
            const string dataspaceIdPreDesign = "predesign";

            const string sysMessage = "system message";
            var userMessageDesign = $"user msg transaction_Id={transactionId}, dataspace={dataspaceIdDesign}";
            var userMessagePreDesign = $"user msg transaction_Id={transactionId}, dataspace={dataspaceIdPreDesign}";

            var dataspaceDesign = new DataspaceInternal()
            {
                Id = dataspaceIdDesign,
                EmailLogLevel = "NOTICE"
            };

            var dataspacePreDesign = new DataspaceInternal()
            {
                Id = dataspaceIdPreDesign,
                EmailLogLevel = "INFO"
            };

            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.RemoveAllAppenders();
            LogHelper.RecordNewTransaction(transactionId, dataspaceDesign);
            LogHelper.RecordNewTransaction(transactionId, dataspacePreDesign);

            var designAppender = hierarchy.Root.GetAppender($"{transactionId}_{dataspaceIdDesign}");
            BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetEntryAssembly()), designAppender);
            Log.Configure(SetupHttpContextAccessor().Object);

            //Logs not captured by any appender
            Log.SetDataspaceId(null);
            Log.SetTransactionId(11);

            Log.Debug(sysMessage);
            Log.Info(sysMessage + "Info");
            Log.Notice(sysMessage + "Notice");
            Log.Warn(sysMessage + "Warn");
            Log.Error(new Exception(sysMessage + "Error"));
            Log.Fatal(new Exception(sysMessage + "Fatal"));

            //Logs not captured by any appender
            Log.SetDataspaceId(dataspaceIdPreDesign);
            Log.SetTransactionId(11);

            Log.Debug(sysMessage);
            Log.Info(sysMessage + "Info");
            Log.Notice(sysMessage + "Notice");
            Log.Warn(sysMessage + "Warn");
            Log.Error(new Exception(sysMessage + "Error"));
            Log.Fatal(new Exception(sysMessage + "Fatal"));

            //Logs captured by design appender
            Log.SetDataspaceId(dataspaceIdDesign);
            Log.SetTransactionId(transactionId);

            Log.Debug(userMessageDesign);
            Log.Info(userMessageDesign + "Info");
            Log.Notice(userMessageDesign + "Notice");
            Log.Warn(userMessageDesign + "Warn");
            Log.Error(new Exception(userMessageDesign + "Error"));
            Log.Fatal(new Exception(userMessageDesign + "Fatal"));


            //Logs captured by PreDesign appender
            Log.SetDataspaceId(dataspaceIdPreDesign);

            Log.Debug(userMessagePreDesign);
            Log.Info(userMessagePreDesign + "Info");
            Log.Notice(userMessagePreDesign + "Notice");
            Log.Warn(userMessagePreDesign + "Warn");
            Log.Error(new Exception(userMessagePreDesign + "Error"));
            Log.Fatal(new Exception(userMessagePreDesign + "Fatal"));

            var designLogEvents = LogHelper.GetRecordedEvents(transactionId, dataspaceIdDesign);

            //Debug and Info messages should not be logged
            Assert.AreEqual(4, designLogEvents.Length);

            Assert.AreEqual(userMessageDesign + "Notice", designLogEvents[0].RenderedMessage);
            Assert.AreEqual(Level.Notice, designLogEvents[0].Level);

            Assert.AreEqual(userMessageDesign + "Warn", designLogEvents[1].RenderedMessage);
            Assert.AreEqual(Level.Warn, designLogEvents[1].Level);

            Assert.AreEqual(userMessageDesign + "Error", designLogEvents[2].RenderedMessage);
            Assert.AreEqual(Level.Error, designLogEvents[2].Level);

            Assert.AreEqual(userMessageDesign + "Fatal", designLogEvents[3].RenderedMessage);
            Assert.AreEqual(Level.Fatal, designLogEvents[3].Level);

            // Appender should be released after the logs have been read
            designAppender = hierarchy.Root.GetAppender($"{transactionId}_{dataspaceIdDesign}");
            Assert.IsNull(designAppender);
            
            var disseminateLogEvents = LogHelper.GetRecordedEvents(transactionId, dataspaceIdPreDesign);

            //Debug messages should not be logged
            Assert.AreEqual(5, disseminateLogEvents.Length);

            Assert.AreEqual(userMessagePreDesign + "Info", disseminateLogEvents[0].RenderedMessage);
            Assert.AreEqual(Level.Info, disseminateLogEvents[0].Level);

            Assert.AreEqual(userMessagePreDesign + "Notice", disseminateLogEvents[1].RenderedMessage);
            Assert.AreEqual(Level.Notice, disseminateLogEvents[1].Level);

            Assert.AreEqual(userMessagePreDesign + "Warn", disseminateLogEvents[2].RenderedMessage);
            Assert.AreEqual(Level.Warn, disseminateLogEvents[2].Level);

            Assert.AreEqual(userMessagePreDesign + "Error", disseminateLogEvents[3].RenderedMessage);
            Assert.AreEqual(Level.Error, disseminateLogEvents[3].Level);

            Assert.AreEqual(userMessagePreDesign + "Fatal", disseminateLogEvents[4].RenderedMessage);
            Assert.AreEqual(Level.Fatal, disseminateLogEvents[4].Level);

            // Appender should be released after the logs have been read
            var disseminateAppender = hierarchy.Root.GetAppender($"{transactionId}_{dataspaceIdPreDesign}");
            Assert.IsNull(disseminateAppender);

            hierarchy.Root.RemoveAllAppenders();
        }
                  
        [Test]
        public void AndFilter()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly());
            hierarchy.Root.RemoveAllAppenders();

            var memoryAppender = new MemoryAppender();
            hierarchy.Root.AddAppender(memoryAppender);

            BasicConfigurator.Configure(LogManager.GetRepository(Assembly.GetEntryAssembly()), memoryAppender);
            Log.Configure(SetupHttpContextAccessor().Object);

            //Accept matching properties
            var filter = new AndFilter { AcceptOnMatch = true };
            filter.AddFilter(new PropertyFilter
            {
                Key = "color_1",
                StringToMatch = "red"
            });
            filter.AddFilter(new PropertyFilter
            {
                Key = "color_2",
                StringToMatch = "blue",
            });
            memoryAppender.AddFilter(filter);
            memoryAppender.AddFilter(new DenyAllFilter());

            Log.Info("colors not matching");

            LogicalThreadContext.Properties["color_1"] = "red";
            LogicalThreadContext.Properties["color_2"] = "blue";
            var msg = "colors matching";
            Log.Info(msg);

            var loggedEvents = memoryAppender.GetEvents();

            Assert.AreEqual(1,loggedEvents.Length);
            Assert.AreEqual(msg, loggedEvents[0].RenderedMessage);
            

            memoryAppender.Clear();
            memoryAppender = new MemoryAppender();
            hierarchy.Root.AddAppender(memoryAppender);

            LogicalThreadContext.Properties["color_1"] = "";
            LogicalThreadContext.Properties["color_2"] = "";
            
            //Reject matching properties
            filter = new AndFilter { AcceptOnMatch = false };
            filter.AddFilter(new PropertyFilter
            {
                Key = "color_1",
                StringToMatch = "red"
            });
            filter.AddFilter(new PropertyFilter
            {
                Key = "color_2",
                StringToMatch = "blue"
            });
            memoryAppender.AddFilter(filter);

            msg = "colors not matching";
            Log.Info(msg);

            LogicalThreadContext.Properties["color_1"] = "red";
            LogicalThreadContext.Properties["color_2"] = "blue";
            Log.Info("colors matching");

            loggedEvents = memoryAppender.GetEvents();

            Assert.AreEqual(1, loggedEvents.Length);
            Assert.AreEqual(msg, loggedEvents[0].RenderedMessage);

            hierarchy.Root.RemoveAllAppenders();
        }

        [Test]
        public void DecideFilter()
        {
            var filter = new AndFilter
            {
                AcceptOnMatch = true,
                Filter = new LevelMatchFilter {LevelToMatch = Level.Debug, AcceptOnMatch = true}
            };

            const string msg = "Debug level";
            var loggingEventData = new LoggingEventData
            {
                Level = Level.Debug,
                Message = msg
            };

            var loggingEvent = new LoggingEvent(loggingEventData);

            Assert.AreEqual(FilterDecision.Accept, filter.Decide(loggingEvent));
            
            filter.Filter = new LevelMatchFilter
            {
                LevelToMatch = Level.Info,
                AcceptOnMatch = true
            };

            Assert.AreEqual(FilterDecision.Neutral, filter.Decide(loggingEvent));

            filter = new AndFilter {AcceptOnMatch = false, Filter = new LevelMatchFilter {LevelToMatch = Level.Debug}};

            Assert.AreEqual(FilterDecision.Deny, filter.Decide(loggingEvent));
            Assert.Throws<ArgumentNullException>(() => filter.Decide(null));
        }

        private static Mock<IHttpContextAccessor> SetupHttpContextAccessor(string httpMethod= "", string path = "", string queryString = "", string referer = "")
        {
            var contextAccessor = new Mock<IHttpContextAccessor>();
            var context = new Mock<HttpContext>();
            var request = new Mock<HttpRequest>();
            request.Setup(x => x.Method).Returns(httpMethod);
            request.Setup(x => x.Path).Returns(path);
            request.Setup(x => x.QueryString).Returns(new QueryString(queryString));
            request.Setup(x => x.Headers["Referer"]).Returns(referer);
            context.Setup(x => x.Request).Returns(request.Object);
            contextAccessor.Setup(x => x.HttpContext).Returns(context.Object);
            return contextAccessor;
        }
    }
}
