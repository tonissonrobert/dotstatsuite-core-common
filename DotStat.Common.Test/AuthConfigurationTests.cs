﻿using System.Collections.Generic;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace DotStat.Common.Test
{
    internal class AuthConfigurationTests
    {
        private ServiceProvider _provider;
        [SetUp]
        public void Setup()
        {
            var testConfiguration = new Dictionary<string, string>
            {
                {"enabled", "true"},
                {"authority", "authorityProvider"},
                {"clientId", "CID"},
                {"authorizationUrl", "url"},
                {"tokenUrl", "token_url"},
                {"scopes:0", "openid"},
                {"scopes:1", "profile"},
                {"scopes:2", "email"},
                {"claimsMapping:email", "admin@email.com"},
                {"claimsMapping:groups", "admins"},
                {"requireHttps", "true"},
                {"validateIssuer", "false"}
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(testConfiguration)
                .Build();

            var services = new ServiceCollection();
            services.AddSingleton(configuration.Get<AuthConfiguration>());
            services.AddTransient<TestAuthConfigurationService>();

            _provider = services.BuildServiceProvider();
        }

        [Test]
        public void TestGeneralConfiguration()
        {
            var testConfigurationService = (TestAuthConfigurationService)_provider.GetService(typeof(TestAuthConfigurationService));
            var config = (IAuthConfiguration)testConfigurationService.GetConfiguration();

            Assert.AreEqual(true, config.Enabled);
            Assert.AreEqual("authorityProvider", config.Authority);
            Assert.AreEqual("CID", config.ClientId);
            Assert.AreEqual("url", config.AuthorizationUrl);
            Assert.AreEqual("token_url", config.TokenUrl);
            Assert.AreEqual(3, config.Scopes.Length);
            Assert.AreEqual("openid", config.Scopes[0]);
            Assert.AreEqual("profile", config.Scopes[1]);
            Assert.AreEqual("email", config.Scopes[2]);

            Assert.AreEqual(true, config.ClaimsMapping.ContainsKey("email"));
            Assert.AreEqual("admin@email.com", config.ClaimsMapping["email"]);
            Assert.AreEqual(true, config.ClaimsMapping.ContainsKey("groups"));
            Assert.AreEqual("admins", config.ClaimsMapping["groups"]);

            Assert.AreEqual(true, config.RequireHttps);
            Assert.AreEqual(false, config.ValidateIssuer);
        }
    }

    internal interface ITestAuthConfigurationService
    {
        AuthConfiguration GetConfiguration();
    }

    internal class TestAuthConfigurationService : ITestAuthConfigurationService
    {
        private readonly AuthConfiguration _authConfiguration;

        public TestAuthConfigurationService(AuthConfiguration configuration)
        {
            _authConfiguration = configuration;
        }

        public AuthConfiguration GetConfiguration()
        {
            return _authConfiguration;
        }
    }
}
