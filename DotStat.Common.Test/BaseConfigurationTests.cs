using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using log4net.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace DotStat.Common.Test
{
    [TestFixture]
    internal class BaseConfigurationTests
    {
        private ServiceProvider _provider;
        [SetUp]
        public void Setup()
        {
            var testConfiguration = new Dictionary<string, string>
            {
                {"DefaultLanguageCode", "xx"},
                {"DotStatSuiteCoreCommonDbConnectionString", "commonDbConnectionString"},
                {"MaxTransferErrorAmount", "0"},
                {"AutoLog2Stdout", "true"},
                {"MailFrom", "mailFrom"},
                {"MaxTextAttributeLength", "200" },
                {"SmtpEnableSsl", "true"},
                {"SmtpHost", "smtpHost"},
                {"SmtpPort", "545"},
                {"SmtpUserName", "username"},
                {"SmtpUserPassword", "pass"},
                {"SmtpHFrom", "smtpHFrom"},
                {"spacesInternal:0:Id", "test"},
                {"spacesInternal:0:DotStatSuiteCoreStructDbConnectionString", "structDbConnectionString"},
                {"spacesInternal:0:DotStatSuiteCoreDataDbConnectionString", "dataDbConnectionString"},
                {"spacesInternal:0:DataImportTimeOutInMinutes", "60"},
                {"spacesInternal:0:DatabaseCommandTimeoutInSec", "60"},
                {"spacesInternal:0:AutoLog2DB", "true"},
                {"spacesInternal:0:AutoLog2DBLogLevel", "Debug"},
                {"spacesInternal:0:EmailLogLevel", "Info"},
                {"showAdvanceDbHealthInfo", "false"},
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(testConfiguration)
                .Build();

            var services = new ServiceCollection();
            services.AddSingleton(configuration.Get<BaseConfiguration>());
            services.AddTransient<TestBaseConfigurationService>();

            _provider = services.BuildServiceProvider();
        }

        [Test]
        public void TestGeneralConfiguration()
        {
            var testConfigurationService = (TestBaseConfigurationService)_provider.GetService(typeof(TestBaseConfigurationService));
            var config = (IGeneralConfiguration)testConfigurationService.GetConfiguration();

            Assert.AreEqual("xx", config.DefaultLanguageCode);
            Assert.AreEqual("commonDbConnectionString", config.DotStatSuiteCoreCommonDbConnectionString);
            Assert.AreEqual(0, config.MaxTransferErrorAmount);
            Assert.AreEqual(true, config.AutoLog2Stdout);
            Assert.AreEqual("DEBUG", config.AutoLog2StdoutLogLevel);
            Assert.AreEqual(200, config.MaxTextAttributeLength);
        }

        [Test]
        public void TestMailConfiguration()
        {
           var testConfigurationService =  (TestBaseConfigurationService)_provider.GetService(typeof(TestBaseConfigurationService));
           var config = (IMailConfiguration)testConfigurationService.GetConfiguration();

           Assert.AreEqual("mailFrom", config.MailFrom);
           Assert.AreEqual(true, config.SmtpEnableSsl);
           Assert.AreEqual("smtpHost", config.SmtpHost);
           Assert.AreEqual(545, config.SmtpPort);
           Assert.AreEqual("username", config.SmtpUserName);
           Assert.AreEqual("pass", config.SmtpUserPassword);
           Assert.AreEqual("smtpHFrom", config.SmtpHFrom);
        }

        [Test]
        public void TestDataspacesConfiguration()
        {
            var testConfigurationService = (TestBaseConfigurationService)_provider.GetService(typeof(TestBaseConfigurationService));
            var config = (IDataspaceConfiguration)testConfigurationService.GetConfiguration();

            Assert.AreEqual(1, config.SpacesInternal.Count);
            Assert.AreEqual(false, config.ShowAdvanceDbHealthInfo);
        }

        [Test]
        public void TestDataspaceInternalConfiguration()
        {
            var testConfigurationService = (TestBaseConfigurationService)_provider.GetService(typeof(TestBaseConfigurationService));
            var config = ((IDataspaceConfiguration)testConfigurationService.GetConfiguration()).SpacesInternal.FirstOrDefault();

            Assert.NotNull(config);
            Assert.AreEqual("test", config.Id);
            Assert.AreEqual("structDbConnectionString", config.DotStatSuiteCoreStructDbConnectionString);
            Assert.AreEqual("dataDbConnectionString", config.DotStatSuiteCoreDataDbConnectionString);
            Assert.AreEqual(60, config.DataImportTimeOutInMinutes);
            Assert.AreEqual(60, config.DatabaseCommandTimeoutInSec);
            Assert.AreEqual(true, config.AutoLog2DB);
            StringAssert.AreEqualIgnoringCase(Level.Debug.Name, config.AutoLog2DBLogLevel);
            Assert.AreEqual(true, config.AllowPITTargetDataVersion);
            StringAssert.AreEqualIgnoringCase(Level.Info.Name, config.EmailLogLevel);
        }

        
        [Test]
        public void TestSecurityConfiguration()
        {
            var testConfigurationService = (TestBaseConfigurationService)_provider.GetService(typeof(TestBaseConfigurationService));

            var eddFileAllowedExtensions = testConfigurationService.GetConfiguration().EddFileAllowedExtensions;
            Assert.NotNull(eddFileAllowedExtensions);
            Assert.AreEqual(1, eddFileAllowedExtensions.Length);
            Assert.AreEqual("xml",eddFileAllowedExtensions[0]);

            var excelFileAllowedExtensions = testConfigurationService.GetConfiguration().ExcelFileAllowedExtensions;
            Assert.NotNull(excelFileAllowedExtensions);
            Assert.AreEqual(1, excelFileAllowedExtensions.Length);
            Assert.AreEqual("xlsx", excelFileAllowedExtensions[0]);

            var SDMXFileAllowedExtensions = testConfigurationService.GetConfiguration().SDMXFileAllowedExtensions;
            Assert.NotNull(SDMXFileAllowedExtensions);
            Assert.AreEqual(3, SDMXFileAllowedExtensions.Length);
            Assert.AreEqual("csv", SDMXFileAllowedExtensions[0]);
            Assert.AreEqual("xml", SDMXFileAllowedExtensions[1]);
            Assert.AreEqual("zip", SDMXFileAllowedExtensions[2]);

            var eddFileAllowedContentTypes = testConfigurationService.GetConfiguration().EddFileAllowedContentTypes;
            Assert.NotNull(eddFileAllowedContentTypes);
            Assert.AreEqual(3, eddFileAllowedContentTypes.Length);
            Assert.AreEqual("application/xml", eddFileAllowedContentTypes[0]);
            Assert.AreEqual("text/xml", eddFileAllowedContentTypes[1]);
            Assert.AreEqual("application/octet-stream", eddFileAllowedContentTypes[2]);

            var excelFileAllowedContentTypes = testConfigurationService.GetConfiguration().ExcelFileAllowedContentTypes;
            Assert.NotNull(excelFileAllowedContentTypes);
            Assert.AreEqual(3, excelFileAllowedContentTypes.Length);
            Assert.AreEqual("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelFileAllowedContentTypes[0]);

            var SDMXFileAllowedContentTypes = testConfigurationService.GetConfiguration().SDMXFileAllowedContentTypes;
            Assert.NotNull(SDMXFileAllowedContentTypes);
            
        }
    }

    internal interface ITestBaseConfigurationService
    {
        BaseConfiguration GetConfiguration();
    }

    internal class TestBaseConfigurationService : ITestBaseConfigurationService
    {
        private readonly BaseConfiguration _baseConfiguration;

        public TestBaseConfigurationService(BaseConfiguration configuration)
        {
            _baseConfiguration = configuration;
        }

        public BaseConfiguration GetConfiguration()
        {
            return _baseConfiguration;
        }
    }

}
