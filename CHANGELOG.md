# HISTORY

## v19.0.0
- [#576](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/576) Added new config and localization for time machine
- [#392](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/392) Updated nuget references to match with ESTAT v8.18.4
- Added configuration for MariaDb columnstore indexes
- Added DbType configuration parameter

## v18.0.0
- [#306](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/306) Added new localization key for confidential observations
- [#374](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/374) Updated nuget references to match with ESTAT v8.18.0
- [#377](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/377) Updated nuget references to match with ESTAT v8.18.2
- [#534](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/534) Added data compression setting


## v17.0.0
- [#95](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/95) Added new localization text for invalid codes of allowed CC
- [#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/124) Removed un-used dependency which contains CRITICAL security issues
- [#126](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/126) MaxTextDimensionLength config added
- [#313](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/313) Updated nuget references to match with ESTAT v8.13.0
- [#325](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/325) Updated nuget references to match with ESTAT v8.15.0
- [#331](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/331) Updated nuget references to match with ESTAT v8.15.1
- [#352](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/352) Added localized text for metadata delete and merge actions
- [#355](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/355) Updated nuget references to match with ESTAT v8.16.0
- [#361](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/361) Updated nuget references to match with ESTAT v8.17.0
- [#464](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/464) Added new localized text for failed init/dataflow
- [#504](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/504) Added localization key error to save ACC
- [#505](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/505) Removed unecessary localized text
- [#518](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/518) Added new validation key 'NoTimeZoneProvidedForUpdatedAfter'
- [#550](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/550) Upgrade of google log4net lib as the old one was failing in alpine linux 
- Updated license information


## v16.0.3
- [#126](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/126) Fix for log records of transactions appearing in two different dataspaces
- [#563](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/563) Private repository replaced to Gitlab


## v16.0.2
- [#450](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/450) Fix for non-unique component ID accross DSD & referenced MSD


## v16.0.1
- [#435](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/435) Added new localized text for missing referenced dimensions


## v16.0.0
- [#127](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/127) New Localized text for delete actions
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) References updated to match with NSI WS v8.12.0
- [#281](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/281) References updated to match with NSI WS v8.12.1
- [#397](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/397) Add new localized validation elements
- [#426](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/426) Added configuration for expected Audience claim in auth token


## v15.0.3
- [#387](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/387) Add optional SMTP HFrom header configuration


## v15.0.2
- [#379](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/379) Add MailSent
- [#382](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/382) Cleanup of orphaned codelists


## v15.0.1
- Disabled resource auto detection of Google logger


## v15.0.0
- [#83](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/83) Upgrade from .NET Core 3.1 to .NET 6
- [#87](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/87) Added ShowAdvanceDbHealthInfo configuration setting
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) ESTAT references updated to v8.11.0
- [#267](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/267) Added log4net google appender
- [#277](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/277) Added logger filter to track events of FireRowsCopiedEvent for memory appender and added localized keys for import logs
- [#291](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/291) Added DotStatSuiteCoreDataDbConnectionStringReadOnlyReplica configuration setting


## v14.1.0
- [#147](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/147) added classes for messaging service
- [#304](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/304) DotStatException added 
- [#278](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/278) Add localized TerminatingCanceledTransaction and MaxConcurrentTransactions 


## v14.0.1
- [#321](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/321) Fix for issue of data transactions failing with dataflows supporting ref.metadata


## v14.0.0
- [#310](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/310) add translations for cleanup 
- [#316](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/316) Add HttpClientTimeOut 
- [#234](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/234) References updated to NSI v8.9.2 
- [#224](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/224) References updated to NSI v8.9.1 
- [#296](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/296) Correct the transfer log messages for Referential Metadata transactions 
- [#293](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/293) Add a proper error description when uploading metadata file to a structure without annotation link to MSD
- [#81](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/81) eurostat packages update to 8.9.0
- [#80](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/80) Add new localised settings for metadata imports 


## v13.0.0
- [#230](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/230) References updated to NSI v8.8.0 


## v12.0.0
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/174) References updated to match Eurostat NSI v8.7.1


## v11.0.0
- [#33](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/33) Added TokenUrl param to auth config
- [#113](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/113) New localization keys added related to support of time at time dimension
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/161) References updated to match Eurostat NSI v8.5.0
- [#178](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/178) Added new validation keys for file upload stream
- [#212](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/212) Add new label for missing Primary measure representation warning
- [#215](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/215) New localized warning for non-included components
- [#224](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/223) Change localization text for bulk copy notifications
- [#231](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/231) Add localization for updated mappingsets
- [#242](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/242) New ApplyEmptyDataDbConnectionStringInMSDB configuration parameter added


## v10.0.0
- [#123](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) Add new translation keys for validations 
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/174) Create transaction item for init/allMappingsets method 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/4) Update Transfer service error messages


## v9.0.0
- [#60](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/60) Csv reader validation error messages 
- [#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/124) Non-numeric measure type implementation related changes 
- [#167](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/167) Localisations for db validation 
- [#157](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/57)Add localization key sdmx-ml groups before series  


## v8.0.0
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/63) New localizations added for better error description during initialization of dataflows and mappingsets, database logger to reconnect on error.  
- [#103](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/103) References updated to match Eurostat NSI v8.1.2. 
- [#110](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/110) Add allowed content constraint check for attributes. 
- [#165](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/165) New localizations for fix of all mappingsets feature.
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/120) New localizations for validate format of submissionStart and submissionEnd parameters. 
- [#154](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/154) Added UnauthorizedImport label for dataflow transfers. 
- [#49](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/49) Add localized messages for automatically create mapping sets feature. 
- [#12](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/12) Use Estat PermissionType.
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/120) User email to logs and localization message for email parameter.  


## v7.1.0
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/102
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/100


## v7.0.0 2020-06-08
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/1
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/94
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/29
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/95
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/83
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93


## v5.2.2 2020-04-17
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/62
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/103
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/65


##  v5.0.1 2020-01-28 
- sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66


##  v4.1.5 2020-01-22
- sis-cc/.stat-suite/dotstatsuite-core-common/issues/102